package com.patpainter.helpers;

import com.patpainter.logic.ornament.OrnamentHandler;
import com.patpainter.model.ornament.OrnamentObject;
import com.patpainter.model.groups.Group;
import com.patpainter.model.shapes.PaintShape;

/*
    ConversionHelper class deals with all types of Conversions.
    It acts as a helper class to offload some work from other classes
 */
public class ConversionHelper {

    private final static ConversionHelper instance = new ConversionHelper();

    private String tabString = "";

    public static ConversionHelper getInstance(){
        return instance;
    }

    public String getTabString(){
        return this.tabString;
    }

    public void incrementTabString(){
        this.tabString += "\t";
    }

    public void decrementTabString(){

        if(this.tabString.substring(this.tabString.length() - 1).equals("\t")){

            //decrement a tab from the string!
            this.tabString = this.tabString.substring(0, this.tabString.length() - 1);
        }
    }

    private ConversionHelper(){}

    public String ConvertShapesToString(PaintShape shape, boolean tabsEnabled){

        String typeObject = "";

        switch (shape.getTypeObject()){
            case ELLIPSE:
                typeObject = "ellipse";
                break;
            case RECTANGLE:
                typeObject = "rectangle";
                break;
            case GROUP:
                typeObject = "group";
        }

        String ornamentString;
        String shapeString;

        //Check for ornament
        if(shape.getClass().equals(OrnamentObject.class)){

            ornamentString = OrnamentHandler.getInstance().OrnamentToString(shape, tabsEnabled);

            if(tabsEnabled){

                shapeString = ornamentString;
                shapeString += this.tabString + typeObject + " " + shape.getPos().getKey() + " " + shape.getPos().getValue() + " "
                        + shape.getSize().getKey() + " " + shape.getSize().getValue() + "\n";
            }else {
                shapeString = ornamentString;
                shapeString += typeObject + " " + shape.getPos().getKey() + " " + shape.getPos().getValue() + " "
                        + shape.getSize().getKey() + " " + shape.getSize().getValue() + "\n";
            }
        }else{

            if(typeObject.equals("group")){

                Group tempGroup = (Group)shape;
                return shapeString = tempGroup.exportGroupToString(true);
            }

            if(tabsEnabled){
                shapeString = this.tabString + typeObject + " " + shape.getPos().getKey() + " " + shape.getPos().getValue() + " "
                        + shape.getSize().getKey() + " " + shape.getSize().getValue() + "\n";
            }else {
                shapeString = typeObject + " " + shape.getPos().getKey() + " " + shape.getPos().getValue() + " "
                        + shape.getSize().getKey() + " " + shape.getSize().getValue() + "\n";
            }
        }

        return shapeString;
    }
}
