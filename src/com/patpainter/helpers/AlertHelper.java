package com.patpainter.helpers;

import javafx.scene.control.Alert;

/*
    AlertHelper class which can show all type of alerts.
 */
public class AlertHelper {

    private static final AlertHelper instance = new AlertHelper();

    public static AlertHelper getInstance(){
        return instance;
    }

    private AlertHelper(){}

    //Specific Alert implementations
    //<editor-fold>

    public void showSelectObjectTypeAlert(){

        //Show warning to user, to select an object type first.
        this.showWarningAlert("Draw Warning", "Please select an Object Type to draw first!");
    }

    public void showEndOfUndoStackAlert(){

        //Show an alert to the user, to inform it has reached the bottom of the undo stack
        this.showWarningAlert("Undo unpossible", "Could not undo, because you reached the end of the stack!");
    }

    //</editor-fold>


    //General Alerts
    //<editor-fold>

    private void showWarningAlert(String title, String message){

        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(title);

        // Header Text: null
        alert.setHeaderText(null);
        alert.setContentText(message);

        alert.showAndWait();
    }
    //</editor-fold>
}
