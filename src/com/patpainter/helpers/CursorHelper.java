package com.patpainter.helpers;

import com.patpainter.Main;
import com.patpainter.PaintAction;
import com.patpainter.PaintObjectCorner;
import com.patpainter.PaintObjectType;
import com.patpainter.model.shapes.PaintShapes;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;

public class CursorHelper {

    private static CursorHelper instance = new CursorHelper();

    public static CursorHelper getInstance(){
        return instance;
    }

    private PaintObjectCorner cursorCorner = PaintObjectCorner.NONE;

    private CursorHelper(){}

    public void checkCursorImage(MouseEvent event){

        //If mouse position is within the bounds of the PaintShape object, display a HAND Cursor
        // and set the PaintAction to MOVE, for a possible move action.
        if(PaintShapes.getInstance().isMouseInBounds(event.getX(), event.getY())){

            Main.mainController.setCanvasCursor(Cursor.HAND);
            Main.mainController.setActionType(PaintAction.MOVE);
        }else{

            Main.mainController.setCanvasCursor(Cursor.DEFAULT);

            if(!Main.mainController.getPaintObjectType().equals(PaintObjectType.NONE)) {
                Main.mainController.setActionType(PaintAction.DRAW);
            }
        }

        //If mouse position is within the Margin area of one of the corners of the PaintShape object,
        // display a RESIZE Cursor, and set the PaintAction to RESIZE for a possible resize action.
        switch (PaintShapes.getInstance().isMouseInCornerBounds(event.getX(), event.getY())){

            case TOPLEFT:
                Main.mainController.setCanvasCursor(Cursor.NW_RESIZE); Main.mainController.setActionType(PaintAction.RESIZE);
                this.cursorCorner = PaintObjectCorner.TOPLEFT;
                break;
            case TOPRIGHT:
                Main.mainController.setCanvasCursor(Cursor.NE_RESIZE); Main.mainController.setActionType(PaintAction.RESIZE);
                this.cursorCorner = PaintObjectCorner.TOPRIGHT;
                break;
            case BOTTOMLEFT:
                Main.mainController.setCanvasCursor(Cursor.NE_RESIZE); Main.mainController.setActionType(PaintAction.RESIZE);
                this.cursorCorner = PaintObjectCorner.BOTTOMLEFT;
                break;
            case BOTTOMRIGHT:
                Main.mainController.setCanvasCursor(Cursor.NW_RESIZE); Main.mainController.setActionType(PaintAction.RESIZE);
                this.cursorCorner = PaintObjectCorner.BOTTOMRIGHT;
                break;
        }
    }
}
