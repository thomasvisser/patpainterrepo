package com.patpainter.helpers;

import com.patpainter.SaveDirectory;
import java.util.ArrayList;
import java.util.List;

public class IOHelper {

    /*
        IOHelper class offers some functionality as an extension for the IOHandler (or other classes)
        It can give you the correct filepath for default local directories.
        It also checks for the validity of the loaded .txt file, to load a drawing
     */
    private static final IOHelper instance = new IOHelper();

    public static IOHelper getInstance(){
        return instance;
    }

    private IOHelper(){}

    /**
     * Returns the pathName for the given directory
     * @param directory the directory to get the System pathName for
     * @return the pathName of the directory
     */
    public String getFilePathFor(SaveDirectory directory){

        switch (directory){
            case DESKTOP:
                return System.getProperty("user.home") + "/Desktop";
            case DOCUMENTS:
                return System.getProperty("user.home") + "/Documents";
            case DOWNLOADS:
                return System.getProperty("user.home") + "/Downloads";
            default:
                return System.getProperty("user.home") + "/Downloads";
        }
    }

    /**
     * Validity checker to check if a string line read from the file
     * is in the correct syntax.
     * @param shapesStringRepresentation the line to check the validity of
     * @return boolean value whether the line is valid yes or no
     */
    public boolean checkValidity(List<String> shapesStringRepresentation){

        for(String line: shapesStringRepresentation) {

            String[] splitLineTemp = line.split("\\s+");
            String[] splitLine = this.cleanResultArray(splitLineTemp);

            //Check for Group
            if (splitLine.length == 2) {

                if (!splitLine[0].equals("group")) {

                    //DEBUG
                    //System.out.println("NOT A GROUP!");
                    return false;
                }

                //Check if the second variable, describing how many Objects
                // in a group can be parsed to Double
                try {
                    Double.parseDouble(splitLine[1]);
                } catch (NumberFormatException e) {

                    //DEBUG
                    //System.out.println("GROUP NUMBER IS NOT VALID!");
                    return false;
                }
            }
            //Check for Ornament
            else if (splitLine.length == 3) {

                if (!splitLine[0].equals("ornament")) {

                    //DEBUG
                    //System.out.println("NOT AN ORNAMENT!");
                    return false;
                }

                //Return false when there is not given a valid ornament positioning
                if (!splitLine[1].equals("top") && !splitLine[1].equals("bottom") &&
                        !splitLine[1].equals("left") && !splitLine[1].equals("right")) {

                    //DEBUG
                    //System.out.println("ORNAMENT POSITIONING IS NOT VALID!");
                    return false;
                }

                if (!splitLine[2].startsWith("\"") && !splitLine[2].endsWith("\"")) {

                    //DEBUG
                    //System.out.println("ORNAMENT DESCRIPTION IS NOT VALID!");
                    return false;
                }
            }
            //Check for PaintShape
            else if (splitLine.length == 5) {

                //Return false when there has been described a wrong PaintShape Object
                if (!splitLine[0].equals("circle") && !splitLine[0].equals("ellipse") &&
                        !splitLine[0].equals("square") && !splitLine[0].equals("rectangle")) {

                    //DEBUG
                    //System.out.println("NOT A VALID PAINTSHAPE!");
                    return false;
                }

                try {
                    Double.parseDouble(splitLine[1]);
                    Double.parseDouble(splitLine[2]);
                    Double.parseDouble(splitLine[3]);
                    Double.parseDouble(splitLine[4]);
                } catch (NumberFormatException e) {

                    //DEBUG
                    //System.out.println("X, Y OR POSITION OF PAINTSHAPE IS NOT VALID!");
                    return false;
                }
            }
        }

        return true;
    }

    public String[] cleanResultArray(String[] result){

        List<String> tempList = new ArrayList<>();

        for(int i = 0; i < result.length; i++){
            if(result[i].equals("")){
                continue;
            }else{
                tempList.add(result[i]);
            }
        }

        String[] finalList = new String[tempList.size()];

        for(int i = 0; i < tempList.size(); i++){
            finalList[i] = tempList.get(i);
        }

        return finalList;
    }
}
