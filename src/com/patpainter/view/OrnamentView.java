package com.patpainter.view;

import com.patpainter.logic.ornament.OrnamentType;
import com.patpainter.logic.ornament.OrnamentHandler;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class OrnamentView {

    private static OrnamentType ornamentPos;
    private static String ornamentTitle = "";

    private static Stage ornamentWindow;
    private static Label errorLabel;

    public static void setError(String message){
        errorLabel.setText(message);
    }

    public static void display(){

        ornamentWindow = new Stage();

        ornamentWindow.setTitle("Add Ornament");
        ornamentWindow.setWidth(300);
        ornamentWindow.setHeight(220);

        //Add Label
        Label titleLabel = new Label();
        titleLabel.setText("Ornament Position:");
        titleLabel.setPadding(new Insets(5));

        //Create radio buttons for the position
        RadioButton leftRadio = new RadioButton("left"); leftRadio.setUserData("left");
        RadioButton rightRadio = new RadioButton("right"); rightRadio.setUserData("right");
        RadioButton topRadio = new RadioButton("top"); topRadio.setUserData("top");
        RadioButton bottomRadio = new RadioButton("bottom"); bottomRadio.setUserData("bottom");

        //ToggleGroup for the RadioButtons
        ToggleGroup toggleRadios = new ToggleGroup();

        leftRadio.setToggleGroup(toggleRadios);
        rightRadio.setToggleGroup(toggleRadios);
        topRadio.setToggleGroup(toggleRadios);
        bottomRadio.setToggleGroup(toggleRadios);

        //Add event handler for Toggle Group
        toggleRadios.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            public void changed(ObservableValue<? extends Toggle> ov,
                                Toggle old_toggle, Toggle new_toggle) {

                selectRadioButton(ov, old_toggle, new_toggle, toggleRadios);
            }
        });

        //Place radiobuttons in a Grid Pane
        GridPane positionGrid = new GridPane();
        positionGrid.add(new Text(), 0, 0, 1, 1);
        positionGrid.add(topRadio, 1, 0, 1, 1);
        positionGrid.add(new Text(), 2, 0, 1, 1);
        positionGrid.add(leftRadio, 0, 1, 1, 1);
        positionGrid.add(new Text(), 1, 1, 1, 1);
        positionGrid.add(rightRadio, 2, 1, 1, 1);
        positionGrid.add(new Text(), 0, 2, 1, 1);
        positionGrid.add(bottomRadio, 1, 2, 1, 1);
        positionGrid.add(new Text(), 2, 2, 1, 1);
        positionGrid.setAlignment(Pos.TOP_CENTER);

        //Text Field Label
        Label textFieldLabel = new Label("Ornament name:");
        textFieldLabel.setAlignment(Pos.TOP_CENTER);
        textFieldLabel.setPadding(new Insets(4, 0, 0, 0));

        //Text Area to enter Ornament Name
        TextField textField = new TextField();
        textField.setMaxWidth(100);

        //Add TextField Listener
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            enteredText(newValue);
        });

        //HBox containing textFielLabel and TextField
        HBox textHBox = new HBox();
        textHBox.getChildren().addAll(textFieldLabel, textField);
        textHBox.setPadding(new Insets(10, 0, 0, 0));
        textHBox.setSpacing(5);
        textHBox.setAlignment(Pos.CENTER);

        //Add button
        Button addButton = new Button("Add");
        addButton.setAlignment(Pos.CENTER);
        addButton.setMinWidth(50);
        addButton.setMinHeight(30);

        //Set Action Event Button
        addButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                addButtonPressed();
            }
        });

        errorLabel = new Label();
        errorLabel.setTextFill(Color.RED);
        errorLabel.setAlignment(Pos.CENTER);

        VBox mainVBox = new VBox(5);
        mainVBox.getChildren().addAll(titleLabel, positionGrid, textHBox, addButton, errorLabel);
        mainVBox.setAlignment(Pos.TOP_CENTER);

        //Create main Window Scene
        Scene scene = new Scene(mainVBox);
        ornamentWindow.setScene(scene);
        ornamentWindow.showAndWait();
    }

    //Ornament Control Event Handlers
    private static void selectRadioButton(ObservableValue<? extends Toggle> ov,
                                   Toggle old_toggle, Toggle new_toggle, ToggleGroup toggleGroup){

        if (toggleGroup.getSelectedToggle() != null) {

            switch (toggleGroup.getSelectedToggle().getUserData().toString()){
                case "top":
                    ornamentPos = OrnamentType.TopOrnament;
                    break;
                case "left":
                    ornamentPos = OrnamentType.LeftOrnament;
                    break;
                case "bottom":
                    ornamentPos = OrnamentType.BottomOrnament;
                    break;
                case "right":
                    ornamentPos = OrnamentType.RightOrnament;
                    break;
                default:
                    ornamentPos = OrnamentType.BottomOrnament;
            }
        }
    }

    private static void enteredText(String newValue){

        ornamentTitle = newValue;
    }

    private static void addButtonPressed(){

        //Error handling
        if(ornamentTitle.equals("")){
            errorLabel.setText("Please enter a valid ornament text!");
            return;
        }

        boolean canAddOrnament;

        //Handle adding Ornament
        canAddOrnament = OrnamentHandler.getInstance().addOrnament(ornamentTitle, ornamentPos);

        //Close window if Ornament can be added
        if(canAddOrnament) {
            ornamentWindow.close();
        }
    }
}
