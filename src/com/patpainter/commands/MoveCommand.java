package com.patpainter.commands;

import com.patpainter.ChangeShapeVisitor;
import com.patpainter.Command;
import com.patpainter.logic.PaintActionHandler;
import com.patpainter.logic.SelectionHandler;
import com.patpainter.logic.ornament.OrnamentHandler;
import com.patpainter.model.groups.Group;
import com.patpainter.model.ornament.OrnamentObject;
import com.patpainter.model.shapes.PaintShape;
import com.patpainter.model.shapes.PaintShapes;
import javafx.util.Pair;

public class MoveCommand implements Command {

    //Reference to the movedObject
    private PaintShape movedObject;

    private boolean commandExecuted = false;

    private double posX;
    private double posY;

    private double oldPosX;
    private double oldPosY;

    private double relativeShapePosX;
    private double relativeShapePosY;

    //Determines whether an object needs to be moved relative to the mouse,
    // instead to be moved based on the mouse coordinates.
    private boolean relativeDragging;

    //The Visitor Object, to visit to change the PaintShape Objects
    private ChangeShapeVisitor visitor = new PaintActionHandler();

    public MoveCommand(double xPos, double yPos, boolean relativeDragging){

        //Store the 'old' aka original pos
        this.oldPosX = xPos;
        this.oldPosY = yPos;

        //Get the selected Shape from PaintActionHandler method
        this.movedObject = SelectionHandler.getInstance().getSelectedObject();

        //Remove selection shape upon moving
        this.movedObject.setShapeSelected(false, true);

        this.relativeDragging = relativeDragging;
    }

    public boolean isCommandExecuted(){
        return this.commandExecuted;
    }

    public boolean equalsShape(PaintShape shape){

        if(shape.equals(this.movedObject)){
            return true;
        }else{
            return false;
        }
    }

    public void setObject(PaintShape shape){
        this.movedObject = shape;
    }

    public void setCommandExecuted(boolean executed){
        this.commandExecuted = executed;
    }

    public void setPos(double newPosX, double newPosY){
        this.posX = newPosX;
        this.posY = newPosY;
    }

    /**
     * Calculates the mouse position to a position relative to the moved shape
     * @param moveXPos the Mouse X Pos
     * @param moveYPos the Mouse Y Pos
     */
    public void calculateRelativeShapePos(double moveXPos, double moveYPos){

        this.relativeShapePosX = moveXPos - this.oldPosX;
        this.relativeShapePosY = moveYPos - this.oldPosY;
    }

    @Override
    public void undo() {

        this.movedObject.setPos(oldPosX, oldPosY);
        PaintShapes.getInstance().redrawShapes();
    }

    @Override
    public void redo() {

        this.movedObject.setPos(posX, posY);
        PaintShapes.getInstance().redrawShapes();
    }

    @Override
    public void execute() {

        if(this.movedObject != null) {
            this.movedObject.move(new Pair<>(this.posX, this.posY),
                    new Pair<>(this.relativeShapePosX, this.relativeShapePosY), this.relativeDragging, this.visitor);
        }

        this.posX = this.movedObject.getPos().getKey();
        this.posY = this.movedObject.getPos().getValue();
    }
}
