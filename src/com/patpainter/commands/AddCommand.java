package com.patpainter.commands;

import com.patpainter.Command;
import com.patpainter.PaintObjectType;
import com.patpainter.logic.DrawShapeFactory;
import com.patpainter.model.shapes.PaintShape;
import com.patpainter.model.shapes.PaintShapes;

public class AddCommand implements Command {

    private PaintShape addedShape;

    private PaintObjectType typeObject;
    private double xPos;
    private double yPos;
    private double size;

    public boolean equalsShape(PaintShape shape){

        if(shape.equals(this.addedShape)){
            return true;
        }else{
            return false;
        }
    }

    public void setObject(PaintShape shape){
        this.addedShape = shape;
    }

    public AddCommand(PaintObjectType type, double x, double y, double size){

        this.typeObject = type;
        this.xPos = x;
        this.yPos = y;
        this.size = size;

        //the size value is the value from the sizeSlider UI Component.
        //If the value is less than 1 (aka, it wants to draw the shape at a size smaller then the baseShapeSize)
        //It replaces the minimal size value, with the baseShapeSize.
        if(size < 1){
            this.size = PaintShapes.baseShapeSize;
        }
    }

    @Override
    public void undo() {

        PaintShapes.getInstance().removeShape(this.addedShape);
        PaintShapes.getInstance().redrawShapes();
    }

    @Override
    public void redo() {

        PaintShapes.getInstance().addShape(this.addedShape);
        PaintShapes.getInstance().redrawShapes();
    }

    @Override
    public void execute() {

        //If type of PaintShape to draw is a Rectangle or Ellipse, double the height
        if (this.typeObject.equals(PaintObjectType.RECTANGLE) || this.typeObject.equals(PaintObjectType.ELLIPSE)) {
            this.addedShape = DrawShapeFactory.getInstance().DrawPaintObject(this.typeObject, this.xPos, this.yPos, this.size, this.size * 2.0);
        }else{
            this.addedShape = DrawShapeFactory.getInstance().DrawPaintObject(this.typeObject, this.xPos, this.yPos, this.size, this.size);
        }
    }
}
