package com.patpainter.commands;

import com.patpainter.Command;
import com.patpainter.Main;
import com.patpainter.helpers.AlertHelper;
import com.patpainter.model.shapes.PaintShape;

import java.util.Stack;

public class CommandManager {

    private final static CommandManager instance = new CommandManager();

    private CommandManager(){}

    private Stack<Command> commandUndoStack = new Stack<>();
    private Stack<Command> commandRedoStack = new Stack<>();

    public static CommandManager getInstance(){
        return instance;
    }

    /**
     * Method which will get executed when any action happens.
     * Actions can be of PaintAction: Add, Move, Resize
     * @param command the command to perform
     */
    public void executeCommand(Command command){

        command.execute();

        //Check if it is of type Command, since then, the commands
        // needs to be added to the undoStack to make undo/redo possible
        if(command instanceof Command){

            //Check if MoveCommand is done yet. If not, don't add the command to
            // the stack, since the position will still be updated
            if(command instanceof MoveCommand){
                if(!((MoveCommand) command).isCommandExecuted()){
                    return;
                }
            }

            //Check if ResizeCommand is done yet. If not, don't add the command to
            // the stack, since the position will still be updated
            if(command instanceof ResizeCommand){
                if(!((ResizeCommand) command).isCommandExecuted()){
                    return;
                }
            }

            commandUndoStack.push(command);

            //If the undo button is disabled, enable it again, so the
            // user can interact with it again, to undo Commands.
            if(Main.mainController.isUndoButtonDisabled()){
                Main.mainController.disableUndoButton(false);
            }

            //Clear redo stack since a new action has occurred, and disable the redo button
            this.commandRedoStack.clear();
            Main.mainController.disableRedoButton(true);
        }
    }

    /**
     * Undo functionality to undo an executed Command (Add, Move, Resize)
     */
    public void undo(){

        if(this.commandUndoStack.size() > 0){

            Command cmd = this.commandUndoStack.pop();
            cmd.undo();

            //Add the popped command to the redo stack
            this.commandRedoStack.push(cmd);

            //if there are no more commands available, disable the undo button,
            // because the user cannot do any more undo operations
            if(this.commandUndoStack.size() == 0){
                Main.mainController.disableUndoButton(true);
            }

            //Since there is at least one command available to redo,
            // enable the redo button again
            if(this.commandRedoStack.size() > 0){
                Main.mainController.disableRedoButton(false);
            }

        }else{

            AlertHelper.getInstance().showEndOfUndoStackAlert();
        }
    }

    /**
     * Redo functionality to redo an performed Command (Add, Move, Resize)
     */
    public void redo(){

        if(this.commandRedoStack.size() > 0){

            Command cmd = this.commandRedoStack.pop();
            cmd.redo();

            //Add the popped command to the undo stack
            this.commandUndoStack.push(cmd);

            //if there are no more commands available, disable the undo button,
            // because the user cannot do any more undo operations
            if(this.commandRedoStack.size() == 0){
                Main.mainController.disableRedoButton(true);
            }

            //Since there is at least one command available to undo,
            // enable the undo button again
            if(this.commandUndoStack.size() > 0){
                Main.mainController.disableUndoButton(false);
            }
        }

    }

    public Command equals(PaintShape shape){

        for(Command command : this.commandRedoStack){

            if(command.equalsShape(shape)){
                return command;
            }
        }

        return null;
    }
}
