package com.patpainter.commands;

import com.patpainter.Command;
import com.patpainter.logic.ornament.OrnamentType;
import com.patpainter.model.ornament.OrnamentObject;
import com.patpainter.model.shapes.PaintShape;
import com.patpainter.model.shapes.PaintShapes;

public class OrnamentCommand implements Command {

    private PaintShape ornamentShape;
    private PaintShape oldShape;

    private String ornamentTitle;
    private OrnamentType type;

    public OrnamentCommand(PaintShape shape, String title, OrnamentType type){

        this.oldShape = shape;

        this.ornamentTitle = title;
        this.type = type;
    }

    @Override
    public boolean equalsShape(PaintShape shape) {

        if(shape.equals(this.ornamentShape)){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public void setObject(PaintShape shape) {
        this.ornamentShape = shape;
    }


    @Override
    public void undo() {

        //Replace Ornament Object with the original PaintShape Object without Ornament
        PaintShapes.getInstance().replaceShape(this.ornamentShape, this.oldShape);
        PaintShapes.getInstance().redrawShapes();
    }

    @Override
    public void redo() {

        //Replace PaintShape Object without ornament with the Ornament Object
        PaintShapes.getInstance().replaceShape(this.oldShape, this.ornamentShape);
        PaintShapes.getInstance().redrawShapes();
    }

    @Override
    public void execute() {

        this.ornamentShape= new OrnamentObject(this.oldShape, ornamentTitle, type);

        //Replace new PaintShape with the PaintShape including ornament in the Object List
        PaintShapes.getInstance().replaceShape(this.oldShape, this.ornamentShape);

        //Redraw Shapes on Canvas
        PaintShapes.getInstance().redrawShapes();
    }
}
