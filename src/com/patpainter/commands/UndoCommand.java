package com.patpainter.commands;

import com.patpainter.Command;
import com.patpainter.model.shapes.PaintShape;

/*
    pattern: Interface Class for the Command Pattern.
    It extends Command Interface, to add undo and redo functionality.
 */
public interface UndoCommand extends Command {

    void undo();
    void redo();

    boolean equalsShape(PaintShape shape);
    void setObject(PaintShape shape);
}
