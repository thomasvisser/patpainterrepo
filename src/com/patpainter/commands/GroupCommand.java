package com.patpainter.commands;

import com.patpainter.Command;
import com.patpainter.PaintObjectType;
import com.patpainter.logic.DrawShapeFactory;
import com.patpainter.logic.SelectionHandler;
import com.patpainter.model.groups.Group;
import com.patpainter.model.shapes.PaintShape;
import com.patpainter.model.shapes.PaintShapes;

import java.util.ArrayList;
import java.util.List;

public class GroupCommand implements Command {

    private List<PaintShape> oldItems = new ArrayList<>();

    private Group groupObject;
    private PaintObjectType typeObject;

    private Command tempCommandHolder;

    public GroupCommand(PaintObjectType type){

        this.typeObject = type;
    }

    public boolean equalsShape(PaintShape shape){

        if(shape.equals(this.groupObject)){
            return true;
        }else{
            return false;
        }
    }

    public void setObject(PaintShape shape){

        try{
            this.groupObject = (Group)shape;
        }catch(ClassCastException e){
            System.out.println("Cast exception GroupCommand!");
        }
    }

    @Override
    public void undo() {

        //Store a reference to the Command Object which has a reference of the current groupObject.
        // This is done to make sure the Command is pointing to the same Group Object after the undo is done.
        // The Undo command dismantles the Group Object in new separate BaseShape Objects, so a reference to
        // the GroupObject by another Command Object is no longer valid.
        this.tempCommandHolder = CommandManager.getInstance().equals(this.groupObject);

        //Add all shapes from the Group individually to the model
        for(PaintShape shape : this.groupObject.getGroupObjects()){
            PaintShapes.getInstance().addShape(shape);

            //Store individual items in the Command, to redo later
            this.oldItems.add(shape);
        }

        //Remove group Object
        PaintShapes.getInstance().removeShape(this.groupObject);
        PaintShapes.getInstance().redrawShapes();
    }

    @Override
    public void redo() {

        this.groupObject = (Group)DrawShapeFactory.getInstance().DrawGroupObject(PaintObjectType.GROUP, this.oldItems);

        //When a redo is performed, there is created a new Group Object based on the separate BaseShape Objects.
        //This new instance will be set to the Command Object which had an original reference to the Group Object.
        if(this.tempCommandHolder != null) {
            this.tempCommandHolder.setObject(this.groupObject);
        }
    }

    @Override
    public void execute() {

        this.groupObject = (Group)DrawShapeFactory.getInstance().DrawGroupObject(PaintObjectType.GROUP, SelectionHandler.getInstance().getSelectedObjects());
    }
}
