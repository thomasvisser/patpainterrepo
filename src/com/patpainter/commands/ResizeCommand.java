package com.patpainter.commands;

import com.patpainter.ChangeShapeVisitor;
import com.patpainter.Command;
import com.patpainter.logic.PaintActionHandler;
import com.patpainter.logic.SelectionHandler;
import com.patpainter.logic.ornament.OrnamentHandler;
import com.patpainter.model.groups.Group;
import com.patpainter.model.ornament.OrnamentObject;
import com.patpainter.model.shapes.PaintShape;
import com.patpainter.model.shapes.PaintShapes;

public class ResizeCommand implements Command {

    //Reference to the resized Object
    private PaintShape resizedObject;

    private boolean commandExecuted = false;

    private double posX;
    private double posY;
    private double width;
    private double height;

    private double oldPosX;
    private double oldPosY;
    private double oldWidth;
    private double oldHeight;

    //The Visitor Object, to visit to change the PaintShape Objects
    private ChangeShapeVisitor visitor = new PaintActionHandler();

    public ResizeCommand(double xPos, double yPos, double width, double height){

        //Store the 'old' aka original pos
        this.oldPosX = xPos;
        this.oldPosY = yPos;
        this.oldWidth = width;
        this.oldHeight = height;

        //Get the selected Shape from PaintActionHandler method
        this.resizedObject = SelectionHandler.getInstance().getSelectedObject();

        //Remove selection shape upon resizing
        this.resizedObject.setShapeSelected(false, true);
    }

    public boolean isCommandExecuted(){
        return this.commandExecuted;
    }

    public boolean equalsShape(PaintShape shape){

        if(shape.equals(this.resizedObject)){
            return true;
        }else{
            return false;
        }
    }

    public void setObject(PaintShape shape){
        this.resizedObject = shape;
    }

    public void setCommandExecuted(boolean executed){
        this.commandExecuted = executed;
    }

    public void setPos(double newPosX, double newPosY){
        this.posX = newPosX;
        this.posY = newPosY;
    }

    @Override
    public void undo() {

        this.resizedObject.setPos(this.oldPosX, this.oldPosY);
        this.resizedObject.setSize(this.oldWidth, this.oldHeight);
        PaintShapes.getInstance().redrawShapes();
    }

    @Override
    public void redo() {

        this.resizedObject.setPos(this.posX, this.posY);
        this.resizedObject.setSize(this.width, this.height);
        PaintShapes.getInstance().redrawShapes();
    }

    @Override
    public void execute() {

        if(this.resizedObject != null) {
            this.resizedObject.resize(this.posX, this.posY, this.visitor);
        }

        this.posX = this.resizedObject.getPos().getKey();
        this.posY = this.resizedObject.getPos().getValue();
        this.width = this.resizedObject.getSize().getKey();
        this.height = this.resizedObject.getSize().getValue();
    }
}
