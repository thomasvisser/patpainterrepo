package com.patpainter.io;

import com.patpainter.IOCommand;
import com.patpainter.IOVisitor;

/*
    pattern: Implementation Class of the Command (Command Pattern) Class.
    This class represents a LoadFile Command, to load a file from the disk,
    and show this on the canvas.
 */
public class LoadFileCommand implements IOCommand {

    private IOVisitor visitor = new IOHandler();

    public LoadFileCommand(){}

    @Override
    public void execute() {

        this.visitor.loadFile();
    }
}
