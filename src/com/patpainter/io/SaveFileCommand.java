package com.patpainter.io;

import com.patpainter.IOCommand;
import com.patpainter.IOVisitor;

/*
    pattern: Implementation Class of the Command (Command Pattern) Class.
    This class represents a SaveFile Command, to save a file to the disk,
    in a internally structured .txt file
 */
public class SaveFileCommand implements IOCommand {

    private IOVisitor visitor = new IOHandler();

    public SaveFileCommand(){}

    @Override
    public void execute() {

        this.visitor.saveFile();
    }
}
