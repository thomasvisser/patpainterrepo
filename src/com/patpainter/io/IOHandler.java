package com.patpainter.io;

import com.patpainter.IOVisitor;
import com.patpainter.Main;
import com.patpainter.PaintObjectType;
import com.patpainter.SaveDirectory;
import com.patpainter.helpers.IOHelper;
import com.patpainter.logic.DrawShapeFactory;
import com.patpainter.logic.ornament.OrnamentHandler;
import com.patpainter.logic.ornament.OrnamentType;
import com.patpainter.model.shapes.PaintShape;
import com.patpainter.model.shapes.PaintShapes;
import javafx.util.Pair;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/*
    IOHandler Class manages all the IO-related tasks, such as
    saving a file to the disk, loading a file from the disk but also the undo and redo functionality.
    pattern: This class operates under the logic of Command Pattern: every function
    pattern: will be called by a Command Pattern Interface Object, to execute the specific logic.
 */
public class IOHandler implements IOVisitor {

    private PrintWriter printWriter;
    private FileWriter fileWriter;

    private SaveDirectory saveDirectory = SaveDirectory.DOWNLOADS;

    //Counter which acts as the number for to add for the fileName.
    //It makes sure will be saved unique files (file1.txt, file2.txt etc.)
    static int fileCounter = 1;

    public IOHandler(){}

    public IOHandler(SaveDirectory directory){

        this.saveDirectory = directory;
    }

    /**
     * Saves all data of the PaintShapes Objects to a .txt file
     * on the hard disk. The data will be stored in a kind of tree
     * structure, so nesting will be clearly visible
     */
    public void saveFile(){

        String fileName = IOHelper.getInstance().getFilePathFor(this.saveDirectory) + "/patPainter" + IOHandler.fileCounter + ".txt";

        while(true) {

            //Check if filename already exists in the current Directory.
            //If yes, increment the fileCounter, to create a unique filename
            File tempFile = new File(fileName);
            if (tempFile.exists()) {

                IOHandler.fileCounter += 1;
                fileName = IOHelper.getInstance().getFilePathFor(this.saveDirectory) + "/patPainter" + IOHandler.fileCounter + ".txt";
            }else{
                break;
            }
        }

        try {
            this.fileWriter = new FileWriter(fileName);
            this.printWriter = new PrintWriter(this.fileWriter);

        }catch(IOException e){
            System.out.println("ERROR: Could not create FileWriter/BufferedWriter with error: " + e);
            return;
        }

        String paintShapesStringRepresentation = PaintShapes.getInstance().exportShapesToString();

        this.printWriter.print(paintShapesStringRepresentation);

        //Close PrintWriter after printing
        this.printWriter.close();
    }

    /**
     * Loads a specific file from the disk, which contains data for a
     * drawing to load on the Canvas.
     */
    public void loadFile(){

        //Indicates whether a read Shape needs to be added as a
        // standalone Shape or as a Group
        boolean loadInGroup = false;

        //Indicates whether a Shape should be loaded with any Ornaments
        boolean addOrnaments = false;
        //Temporary list with Ornament Details
        List<Pair<OrnamentType, String>> ornamentList = new ArrayList<>();

        //Temporary List with Shape Objects to be added to a Group.
        List<PaintShape> tempGroupList = new ArrayList<>();
        //temporary stack a counter of the amount of shapes to draw for every group.
        Stack<Integer> groupCounter = new Stack();

        int amountLinesInGroup = 0;
        int currentGroupLineCounter = 0;

        //Open the FileChooser from the UI
        File loadedFile = Main.openFileDialog();

        try {
            List<String> shapesStringRepresentation = Files.readAllLines(loadedFile.toPath());

            //Check validity
            if(!IOHelper.getInstance().checkValidity(shapesStringRepresentation)){
                return;
            }

            //Clear Canvas
            PaintShapes.getInstance().clearShapes();
            PaintShapes.getInstance().redrawShapes();

            //Read data from file. It reads every string line from the file,
            // where every line represents a Shape Object. This can be a Group,
            // Ornament, or a PaintShape Object with a position, and size.
            for(String line: shapesStringRepresentation){

                int startDataIndex;

                String[] splitLineTemp = line.split("\\s+");
                String[] splitLine = IOHelper.getInstance().cleanResultArray(splitLineTemp);

                //Extract type of object, the data needs to be loaded of
                String typeObject = splitLine[0];

                if(typeObject.equals("group")){

                    //Set to true, to indicate the next shapes need to be added to a group
                    loadInGroup = true;
                    amountLinesInGroup = Integer.parseInt(splitLine[1]);

                    groupCounter.push(amountLinesInGroup);

                }else if(typeObject.equals("ornament")){

                    startDataIndex = 1;

                    addOrnaments = true;

                    OrnamentType type;

                    //Extract Ornament Type
                    switch (splitLine[startDataIndex]) {
                        case "left":
                            type = OrnamentType.LeftOrnament; break;
                        case "right":
                            type = OrnamentType.RightOrnament; break;
                        case "top":
                            type = OrnamentType.TopOrnament; break;
                        case "bottom":
                            type = OrnamentType.BottomOrnament; break;
                            default:
                                type = OrnamentType.BottomOrnament; break;
                    }

                    //Extract Ornament title
                    String ornName = splitLine[startDataIndex + 1];

                    ornamentList.add(new Pair<>(type, ornName));

                }else if(typeObject.equals("ellipse") || typeObject.equals("rectangle")){

                    startDataIndex = 1;

                    //Extract data from the read line
                    double xPos = Double.parseDouble(splitLine[startDataIndex]);
                    double yPos = Double.parseDouble(splitLine[startDataIndex + 1]);
                    double width = Double.parseDouble(splitLine[startDataIndex + 2]);
                    double height = Double.parseDouble(splitLine[startDataIndex + 3]);

                    PaintObjectType tempObjectType;

                    switch (typeObject){
                        case "ellipse":
                            tempObjectType = PaintObjectType.ELLIPSE;
                            break;
                        case "rectangle":
                            tempObjectType = PaintObjectType.RECTANGLE;
                            break;
                            default:
                                tempObjectType = PaintObjectType.NONE;
                    }

                    //Check whether the object is part of a Group or not.
                    // If it is not part of a Group, draw the Object on the Canvas.
                    // If the Object IS part of a Group, temporarily store the information
                    // which Objects are part of a Group, and draw them all later.
                    if(!loadInGroup && !addOrnaments) {

                        //Draw new PaintShape based on the data read from the file
                        DrawShapeFactory.getInstance().DrawPaintObject(tempObjectType, xPos, yPos, width, height);
                    }
                    //Check whether the there needs to be added any Ornaments to the read Shape
                    else if(addOrnaments){

                        //reset checker
                        addOrnaments = false;

                        //Draw new PaintShape based on the data read from the file
                        PaintShape shapeToDraw = DrawShapeFactory.getInstance().
                                DrawPaintObject(tempObjectType, xPos, yPos, width, height);

                        //Add Ornaments to the shape
                        for(Pair<OrnamentType, String> ornament : ornamentList){

                            //Remove "" quotes from the title
                            String newOrnTitle = ornament.getValue().substring(1,
                                    ornament.getValue().length() - 1);

                            shapeToDraw = OrnamentHandler.getInstance().
                                    addOrnamentFromFile(shapeToDraw, newOrnTitle, ornament.getKey());
                        }
                    //Check if the read shape needs to be drawn into a group.
                    }else{

                        if(groupCounter.size() > 0){

                            //Draw new PaintShape based on the data read from the file
                            tempGroupList.add(DrawShapeFactory.getInstance().DrawPaintObject(tempObjectType, xPos,
                                    yPos, width, height));

                            currentGroupLineCounter++;

                            //If it read all shapes from 1 group, draw the group to te screen
                            // and reset the counters
                            if(currentGroupLineCounter == groupCounter.peek()){

                                //Create Group from Objects in temp Group List
                                PaintShape tempGroup = DrawShapeFactory.getInstance().DrawGroupObject(PaintObjectType.GROUP, tempGroupList);

                                loadInGroup = false;
                                currentGroupLineCounter = 0;

                                tempGroupList.clear();

                                //Add any Ornaments
                                if(addOrnaments){

                                    //reset checker
                                    addOrnaments = false;

                                    //Add Ornaments to the shape
                                    for(Pair<OrnamentType, String> ornament : ornamentList){

                                        tempGroup = OrnamentHandler.getInstance().
                                                addOrnamentFromFile(tempGroup, ornament.getValue(), ornament.getKey());
                                    }

                                }

                                //Remove group item
                                groupCounter.pop();
                            }
                        }
                    }
                }
            }

        }catch(IOException e){
            System.out.println("ERROR: Could not read contents of loaded file with error: " + e);
        }
    }
}
