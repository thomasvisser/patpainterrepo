package com.patpainter;

/*
    pattern: This Class acts as a Visitor Class for the Visitor Pattern.
    pattern: It represents the Base Visitor Class which can be implemented by a Concrete Class
    pattern: to implement IO operations (Save and Load) functionality.
    pattern: IOHandler will act as the Visitor Implementation class.
 */
public interface IOVisitor {

    void saveFile();
    void loadFile();
}
