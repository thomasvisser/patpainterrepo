package com.patpainter.controller;

import com.patpainter.PaintAction;
import com.patpainter.PaintObjectType;
import com.patpainter.SaveDirectory;
import com.patpainter.commands.*;
import com.patpainter.helpers.AlertHelper;
import com.patpainter.helpers.CursorHelper;
import com.patpainter.io.IOHandler;
import com.patpainter.io.LoadFileCommand;
import com.patpainter.io.SaveFileCommand;
import com.patpainter.logic.SelectionHandler;
import com.patpainter.view.OrnamentView;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.util.Pair;

/*
    PainterUIController represents the main Controller of the main User Interface (PainterUI.fxml)
    It contains all the UI-elements references, on-click actions etc.

    This Controller also acts as the bridge between the Model and the User Interface of the application.
 */
public class PainterUIController {

    //UI-Elements Reference objects
    //<editor-fold>

    //Painting Canvas
    @FXML
    private Canvas paintCanvas;

    //Shape Buttons
    @FXML
    private Button circleButton;
    @FXML
    private Button ellipseButton;
    @FXML
    private Button squareButton;
    @FXML
    private Button rectangleButton;

    //Size Slider
    @FXML
    private Slider sizeSlider;

    //Action Buttons
    @FXML
    private Button groupButton;
    @FXML
    private Button ornamentButton;
    @FXML
    private Button undoButton;
    @FXML
    private Button redoButton;

    //I/O Buttons
    @FXML
    private Button saveButton;
    @FXML
    private Button loadButton;
    //</editor-fold>

    //Controller properties============================================================
    //<editor-fold>

    private GraphicsContext graphicsContext;

    private PaintObjectType typePaintObject = PaintObjectType.NONE;
    private double paintObjectSize = 0;

    private PaintAction actionMode = PaintAction.NONE;

    private MoveCommand moveCommand;
    private ResizeCommand resizeCommand;

    //Keeps track of whether dragging is activated or not
    private boolean dragActivated = false;
    //</editor-fold>

    //Controller properties Getters/Setters============================================
    //<editor-fold>

    public void setCanvasCursor(Cursor value){

        this.paintCanvas.setCursor(value);
    }

    public void setActionType(PaintAction action){

        this.actionMode = action;
    }

    public GraphicsContext getGraphicsContext(){

        return this.graphicsContext;
    }

    //Gets the size of a PaintShape Object to be drawn
    public double getPaintObjectSize(){

        return this.paintObjectSize;
    }

    //Gets the type of Paint Object
    public PaintObjectType getPaintObjectType(){

        return this.typePaintObject;
    }

    //disables/enables the undo button
    public void disableUndoButton(boolean disable){

        this.undoButton.setDisable(disable);
    }

    public boolean isUndoButtonDisabled(){

        return this.undoButton.isDisabled();
    }

    //disabled/enables the redo button
    public void disableRedoButton(boolean disable){

        this.redoButton.setDisable(disable);
    }

    //disables/enables the ornament button
    public void disableOrnamentButton(boolean disable){

        this.ornamentButton.setDisable(disable);
    }

    //disables/enables the group button
    public void disableGroupButton(boolean disable){

        this.groupButton.setDisable(disable);
    }

    //Clears the whole Canvas screen
    public void clearCanvas(){

        this.graphicsContext.clearRect(0, 0, this.paintCanvas.getWidth(), this.paintCanvas.getHeight());
    }

    //</editor-fold>

    //The JavaFX Initializer for the Controller=========================================
    @FXML
    private void initialize()
    {
        //Set the GraphicsContext for the Canvas
        this.graphicsContext = this.paintCanvas.getGraphicsContext2D();

        //Set Drag EventHandler manually
        this.paintCanvas.setOnMouseDragged(event -> this.paintCanvasDragging(event));
    }

    //On-Click Action EventHandlers=====================================================
    //<editor-fold>

    @FXML
    private void circleButtonPressed(){

        //Set type of PaintObjectType
        this.typePaintObject = PaintObjectType.CIRCLE;
        this.actionMode = PaintAction.DRAW;

        SelectionHandler.getInstance().deselectCurrentSelectedObjects(true);
    }

    @FXML
    private void ellipseButtonPressed(){

        //Set type of PaintObjectType and PaintAction
        this.typePaintObject = PaintObjectType.ELLIPSE;
        this.actionMode = PaintAction.DRAW;

        SelectionHandler.getInstance().deselectCurrentSelectedObjects(true);
    }

    @FXML
    private void rectangleButtonPressed(){

        //Set type of PaintObjectType and PaintAction
        this.typePaintObject = PaintObjectType.RECTANGLE;
        this.actionMode = PaintAction.DRAW;

        SelectionHandler.getInstance().deselectCurrentSelectedObjects(true);
    }

    @FXML
    private void squareButtonPressed(){

        //Set type of PaintObjectType and PaintAction
        this.typePaintObject = PaintObjectType.SQUARE;
        this.actionMode = PaintAction.DRAW;

        SelectionHandler.getInstance().deselectCurrentSelectedObjects(true);
    }

    //Action controls

    @FXML
    private void groupButtonPressed(){

        //Set type of PaintObjectType
        this.typePaintObject = PaintObjectType.GROUP;

        //Encapsulate the adding of a Group as an AddCommand with a position and size of 0.0,
        // since the position and the size will be calculated after adding Shapes to the Group
        GroupCommand createGroupCommand = new GroupCommand(this.typePaintObject);
        CommandManager.getInstance().executeCommand(createGroupCommand);

        this.typePaintObject = PaintObjectType.NONE;
        this.actionMode = PaintAction.NONE;
    }

    @FXML
    private void ornamentButtonPressed(){

        //Open Ornament Window
        OrnamentView.display();
    }

    //Undo and Redo controls

    @FXML
    private void undoButtonPressed(){

        CommandManager.getInstance().undo();
    }

    @FXML
    private void redoButtonPressed(){

        CommandManager.getInstance().redo();
    }
    //</editor-fold>

    //Canvas EventHandlers
    //<editor-fold>

    @FXML
    private void paintCanvasPressed(MouseEvent event){

        //DEBUG
        System.out.println("Canvas pressed!!");

        //Check if a new PaintShape needs to be drawn
        if(!this.typePaintObject.equals(PaintObjectType.NONE) && this.actionMode.equals(PaintAction.DRAW)){

            AddCommand addShapeCommand = new AddCommand(this.typePaintObject, event.getX(), event.getY(), this.sizeSlider.getValue());
            CommandManager.getInstance().executeCommand(addShapeCommand);
        }

        //Check if any Warning/Error Messages needs to be displayed
        if((this.typePaintObject.equals(PaintObjectType.NONE)) && (this.actionMode.equals(PaintAction.NONE))){

            //Show alert to user to select an object type to draw first
            AlertHelper.getInstance().showSelectObjectTypeAlert();
        }

        if(this.actionMode.equals(PaintAction.SELECT)){

            SelectionHandler.getInstance().deselectIfMouseNotInBounds(event.getX(), event.getY());
        }
    }

    /**
     * Gets executed when the Touch Event on the canvas has started
     * @param event the MouseEvent Handler from the source
     */
    @FXML
    private void paintCanvasTouchBegan(MouseEvent event){

        //todo: Move this functionality to the MouseMove function?
        //Check which object is selected by the mousePointer
        if(!this.actionMode.equals(PaintAction.NONE)){

            SelectionHandler.getInstance().checkWhichObjectSelected(event);
        }
    }

    /**
     * Gets executed when the Touch Event on the canvas has ended
     */
    @FXML
    private void paintCanvasTouchEnded(){

        //DEBUG
        System.out.println("Paint Canvas Touch Ended!");

        this.dragActivated = false;

        //Update the status of the MoveCommand. After the MoveCommand is done
        // set the executedCommand property to true, to indicate that the command
        // can be stored in the Command Stack
        if(this.moveCommand != null){
            this.moveCommand.setCommandExecuted(true);
            CommandManager.getInstance().executeCommand(this.moveCommand);

            //clean up
            this.moveCommand = null;
        }
        else if(this.resizeCommand != null){
            this.resizeCommand.setCommandExecuted(true);
            CommandManager.getInstance().executeCommand(this.resizeCommand);

            //clean up
            this.resizeCommand = null;
        }

        if(SelectionHandler.getInstance().getSelectedObjectsCount() > 0){
            SelectionHandler.getInstance().highlighSelectedObjects();
            this.actionMode = PaintAction.SELECT;

            this.typePaintObject = PaintObjectType.NONE;
        }
    }

    /**
     * Gets executed when a drag on the canvas has started
     * @param event the MouseEvent Handler from the source
     */
    @FXML
    private void paintCanvasDragBegan(MouseEvent event){

        //DEBUG
        System.out.println("Paint Canvas Drag Begin!");

        this.dragActivated = true;

        if(this.actionMode.equals(PaintAction.MOVE)){

            //Create a new MoveCommand upon DragBegin, to get the very first position
            // of the Move Action. This is important, because the very first position of the
            // MoveAction is necessary to restore to that position
            Pair<Double, Double> initialPos = SelectionHandler.getInstance().getInitialSelObjPos();
            this.moveCommand = new MoveCommand(initialPos.getKey(), initialPos.getValue(), true);
            this.moveCommand.setCommandExecuted(false);

            this.moveCommand.calculateRelativeShapePos(event.getX(), event.getY());
        }
        else if(this.actionMode.equals(PaintAction.RESIZE)){

            //Create a new ResizeCommand upon DragBegin, to get the very first position/size
            // of the Resize Action. This is important, because the very first position/size of the
            // ResizeAction is necessary to restore to that position/size
            Pair<Double, Double> initialPos = SelectionHandler.getInstance().getInitialSelObjPos();
            Pair<Double, Double> initialSize = SelectionHandler.getInstance().getInitialSelObjSize();
            this.resizeCommand = new ResizeCommand(initialPos.getKey(), initialPos.getValue(),
                    initialSize.getKey(), initialSize.getValue());
            this.resizeCommand.setCommandExecuted(false);
        }
    }

    @FXML
    private void paintCanvasMouseMoved(MouseEvent event){

        //Check if mouse cursor needs to be changed
        CursorHelper.getInstance().checkCursorImage(event);
    }

    /**
     * Gets executed for every Drag Move it detects on the Canvas
     * This function will be used for functionality which needs to be updated
     * in realtime of the drag.
     * @param event the MouseEvent Handler from the Source
     */
    public void paintCanvasDragging(MouseEvent event){

        //Check for Drag Paint Actions
        if(this.dragActivated && this.actionMode.equals(PaintAction.MOVE)){

            if(this.moveCommand != null) {
                CommandManager.getInstance().executeCommand(this.moveCommand);
                this.moveCommand.setPos(event.getX(), event.getY());

            }else{

                //DEBUG
                System.out.println("ERROR: MoveCommand is null in Controller?!");
            }

        }else if(this.dragActivated && this.actionMode.equals(PaintAction.RESIZE)){

            if(this.resizeCommand != null) {
                CommandManager.getInstance().executeCommand(this.resizeCommand);
                this.resizeCommand.setPos(event.getX(), event.getY());

            }else{

                //DEBUG
                System.out.println("ERROR: ResizeCommand is null in Controller?!");
            }
        }
    }
    //</editor-fold

    //Slider EventHandler
    @FXML
    private void sizeSliderDragged(){

        this.paintObjectSize = this.sizeSlider.getValue();
    }

    //Load and Save functionality
    //<editor-fold>
    @FXML
    private void saveButtonClicked(){

        SaveFileCommand saveCommand = new SaveFileCommand();
        saveCommand.execute();
    }

    @FXML
    private void loadButtonClicked(){

        LoadFileCommand loadCommand = new LoadFileCommand();
        loadCommand.execute();
    }
    //</editor-fold>

}
