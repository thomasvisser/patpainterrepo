package com.patpainter;

import com.patpainter.controller.PainterUIController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

public class Main extends Application {

    //The fixed width and height of the Application Scene
    public final static int sceneWidth = 1020;
    public final static int sceneHeight = 700;

    public static PainterUIController mainController;

    private static Stage primaryStage;

    //Attaches the File Chooser to the Main Stage and opens the File Chooser.
    // This happens here because the FileChooser Object must be attached to the main Stage of the UI.
    // This is only privately accessible from within the Main Class
    public static File openFileDialog(){

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select Painting File");

        //Add a .txt filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);

        return fileChooser.showOpenDialog(primaryStage);
    }

    public static void main(String[] args) {

        System.out.println("main Started!");

        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{

        //Loads the UI file from XML
        FXMLLoader loader = new FXMLLoader(getClass().getResource("./view/painterUI.fxml"));

        //Create new Controller instance
        mainController = new PainterUIController();

        //Attach controller to the UI file
        loader.setController(mainController);

        Parent root = loader.load();

        primaryStage.setTitle("PatPainter");
        primaryStage.setScene(new Scene(root, Main.sceneWidth, Main.sceneHeight));
        primaryStage.show();

        //Don't allow resizable window
        primaryStage.setResizable(false);

        //Create reference
        Main.primaryStage = primaryStage;
    }
}
