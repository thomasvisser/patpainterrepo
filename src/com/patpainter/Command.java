package com.patpainter;

import com.patpainter.model.shapes.PaintShape;

/*
    pattern: Base Interface Class for the Command Pattern.
    pattern: It describes a simple Command Pattern with an execute function.
 */
public interface Command {

    void execute();
    void undo();
    void redo();

    boolean equalsShape(PaintShape shape);
    void setObject(PaintShape shape);
}
