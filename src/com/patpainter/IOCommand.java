package com.patpainter;

public interface IOCommand {
    void execute();
}
