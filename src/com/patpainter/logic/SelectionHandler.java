package com.patpainter.logic;

import com.patpainter.Main;
import com.patpainter.model.shapes.PaintShape;
import com.patpainter.model.shapes.PaintShapes;
import javafx.scene.input.MouseEvent;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

/*
    SelectionHandler handles the selection of PaintShapes, and thereby
    keeps track of the selectedObjects.
 */
public class SelectionHandler {

    private static final SelectionHandler instance = new SelectionHandler();
    public static SelectionHandler getInstance() { return instance; }

    private List<PaintShape> selectedObjects = new ArrayList<>();

    private boolean didDeselectItem = false;

    public int getSelectedObjectsCount() { return this.selectedObjects.size(); }

    public List<PaintShape> getSelectedObjects() { return this.selectedObjects; }

    public void addSelectedObject(PaintShape shape){

        if(!this.selectedObjects.contains(shape)){
            this.selectedObjects.add(shape);

            //Enable the Ornament button whenever a new PaintShape object is selected.
            Main.mainController.disableOrnamentButton(false);

            //Enable the Group button whenever there are a minimum of 2 Objects selected
            if(this.selectedObjects.size() >= 2){
                Main.mainController.disableGroupButton(false);
            }
        }
    }

    public void replaceSelectedShape(PaintShape oldShape, PaintShape newShape){

        if(this.selectedObjects.contains(oldShape)){
            this.selectedObjects.remove(oldShape);

            this.selectedObjects.add(newShape);
        }
    }

    public void removeSelectedObject(PaintShape shape){

        if(this.selectedObjects.contains(shape)){
            this.selectedObjects.remove(shape);
        }
    }

    /**
     * This function is designed to return the currently selected Object if there is
     * only 1 selected Object present in the selected Object list
     * @return the only selected Object in the Object selection list
     */
    public PaintShape getSelectedObject(){

        if(this.selectedObjects.size() > 0){
            return this.selectedObjects.get(0);
        }

        return null;
    }

    /**
     * This function is designed to return the position of the currently selected Object
     * if there is only 1 selected Object present in the selected Object list
     * @return the position of the only selected Object in the Object selection list
     */
    public Pair<Double, Double> getInitialSelObjPos(){

        if(this.selectedObjects.size() > 0){
            return this.selectedObjects.get(0).getPos();
        }

        return null;
    }

    /**
     * This function is designed to return the size of the currently selected Object
     * if there is only 1 selected Object present in the selected Object list
     * @return the size of the only selected Object in the Object selection list
     */
    public Pair<Double, Double> getInitialSelObjSize(){

        if(this.selectedObjects.size() > 0){
            return this.selectedObjects.get(0).getSize();
        }

        return null;
    }

    private SelectionHandler(){}

    /**
     * Checks if a PaintObject is selected. It also checks when the mouse will hover over the
     * PaintObject to update the mouse cursor images.
     */
    public void checkWhichObjectSelected(MouseEvent event){

        PaintShape selectedShape = PaintShapes.getInstance().getSelectedShape(event.getX(), event.getY());

        if(selectedShape != null){

            //Deselect the currently selected shape only if the control key is not pressed.
            //If the control key is pressed the user can select multiple shapes
            if (this.selectedObjects.size() > 0 && !event.isControlDown()) {

                //DEBUG
                //System.out.println("\nDeselect current object!\n");

                for(PaintShape shape : this.selectedObjects) {
                    this.deselectObject(shape, true);
                }

                this.selectedObjects.clear();
            }

            //Check if Object already is Selected. If yes, remove it from the selected Objects list
            if(this.selectedObjects.contains(selectedShape)){

                //DEBUG
                //System.out.println("Deselect already selected Object!");

                this.deselectObject(selectedShape, true);
                this.selectedObjects.remove(selectedShape);

                this.didDeselectItem = true;

            }else{

                //Add Object to the list with selectedObjects
                this.addSelectedObject(selectedShape);

                selectedShape.setShapeSelected(true, true);
            }

            //DEBUG
            //System.out.println("Amount of selected objects: " + this.selectedObjects.size());
        }

        this.checkUIButtons();
    }

    /**
     * Deselects the single currently selected Object from the list with selected Objects.
     * This function is designed for interaction from outside the PaintActionHandler class,
     * so you don't need to pass a PaintShape to deselect
     */
    public void deselectCurrentSelectedObjects(boolean redrawScreen){

        for(PaintShape shape : this.selectedObjects) {
            this.deselectObject(shape, redrawScreen);
        }

        this.selectedObjects.clear();

        this.checkUIButtons();
    }

    /**
     * Deselects the selected PaintShape Objects, where the SelectionObject will get removed
     * It also deletes the selected Object from the list with Selected Objects
     */
    private void deselectObject(PaintShape shape, boolean redrawScreen){

        shape.setShapeSelected(false, redrawScreen);

        this.checkUIButtons();
    }

    /**
     * Deselects all the objects when the user clicks out of bounds of any selected object
     * @param posX the X position of the mouse pointer
     * @param posY the Y position of the mouse pointer
     */
    public void deselectIfMouseNotInBounds(double posX, double posY){

        if(this.didDeselectItem){
            this.didDeselectItem = false;
            return;
        }

        List<PaintShape> shapesToRemove = new ArrayList<>();

        boolean doRemoveSelectedItems = true;

        for(PaintShape shape: this.selectedObjects){

            if(shape.checkMouseIsInBounds(posX, posY)){
                doRemoveSelectedItems = false;
            }
        }

        //Deselect the necessary shapes
        if(doRemoveSelectedItems) {

            for (PaintShape shape : this.selectedObjects) {
                this.deselectObject(shape,true);
                shapesToRemove.add(shape);
            }

            this.selectedObjects.clear();
        }

        this.checkUIButtons();
    }

    /**
     * Draws a border around every selected Shape, to indicate it is selected
     */
    public void highlighSelectedObjects(){

        for(PaintShape shape : this.selectedObjects) {

            shape.setShapeSelected(true, true);
        }
    }

    /**
     * Checks if the Ornament Button needs to be disabled, if all the selectObjects
     * are cleared
     */
    private void checkUIButtons(){

        //Disable the Ornament button if no PaintShape items are selected OR
        // if there are multiple PaintShapes selected
        if(this.selectedObjects.size() == 0 || this.selectedObjects.size() > 1) {
            Main.mainController.disableOrnamentButton(true);
        }else{
            Main.mainController.disableOrnamentButton(false);
        }

        //Disable the Group button if there are less than 2 Objects selected, since
        // creating a Group with 0/1 objects makes no sense
        if(this.selectedObjects.size() < 2){
            Main.mainController.disableGroupButton(true);
        }
    }
}
