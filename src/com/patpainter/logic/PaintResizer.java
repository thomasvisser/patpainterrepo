package com.patpainter.logic;

import com.patpainter.model.groups.Group;
import com.patpainter.model.shapes.PaintShape;
import com.patpainter.model.shapes.PaintShapes;

public class PaintResizer {

    private static final PaintResizer instance = new PaintResizer();

    private PaintShape selectedObject;

    public static PaintResizer getInstance(){
        return instance;
    }

    private PaintResizer(){}

    public PaintShape resizeShape(double posX, double posY, PaintShape selectedObject){

        double selObjWidth = selectedObject.getSize().getKey();
        double selObjHeight = selectedObject.getSize().getValue();

        //Pick xValue and yValue
        double xValueRight = selectedObject.getRightSideX() - selectedObject.getCornerMargin();
        double xValueLeft = selectedObject.getLeftSideX() + selectedObject.getCornerMargin();
        double yValueTop = selectedObject.getTopSideY() + selectedObject.getCornerMargin();
        double yValueBottom = selectedObject.getBottomSideY() - selectedObject.getCornerMargin();

        //DEBUG
        /*System.out.println("xValueRight: " + xValueRight);
        System.out.println("xValueLeft: " + xValueLeft);
        System.out.println("xValueTop: " + yValueTop);
        System.out.println("yValueBottom: " + yValueBottom);*/

        //Set private selectedObject
        this.selectedObject = selectedObject;

        //RESIZING PART=======================
        //Top left area
        if(posX < xValueRight && posY < yValueBottom){

            this.topLeftResize(posX, posY, selObjWidth, selObjHeight);
        }
        //Top right area
        else if(posX >= xValueRight && posY < yValueBottom){

            this.topRightResize(posX, posY, selObjHeight);
        }
        //Bottom left area
        else if(posX < xValueRight && posY >= yValueTop){

            this.bottomLeftResize(posX, posY, selObjWidth, selObjHeight);
        }
        //Bottom right area
        else if(posX >= xValueRight && posY >= yValueTop){

            this.bottomRightResize(posX, posY);
        }
        //====================================

        //Redraw all shapes on the Canvas (aka redraw the whole screen)
        PaintShapes.getInstance().redrawShapes();

        return this.selectedObject;
    }

    public PaintShape resizeShape(double posX, double posY, double width, double height, PaintShape selectedObject){

        //Change x and y position of the Object to resize
        selectedObject.setPos((double)Math.round(posX * 100000000d) / 100000000d,
                (double)Math.round(posY * 100000000d) / 100000000d);

        //Change size of the Object to resize
        selectedObject.setSize((double)Math.round(width * 100000000d) / 100000000d,
                (double)Math.round(height * 100000000d) / 100000000d);

        return selectedObject;
    }

    private void topLeftResize(double posX, double posY, double selObjWidth, double selObjHeight){

        double tempBottomLeftPosX = this.selectedObject.getBottomLeftPos().getKey();
        double tempTopLeftPosY = this.selectedObject.getTopLeftPos().getValue();

        //Change x and y position of the Object to resize
        this.selectedObject.setPos(posX, posY);

        //DEBUG
        //System.out.println("TOP LEFT resize.    Mouse X: " + posX + ", Mouse Y: " + posY);

        //Change size of the Object to resize
        this.selectedObject.setSize(selObjWidth + (tempBottomLeftPosX - posX),
                selObjHeight + (tempTopLeftPosY - posY));
    }

    private void topRightResize(double posX, double posY, double selObjHeight){

        double tempPosX = this.selectedObject.getPos().getKey();
        double tempTopRightY = this.selectedObject.getTopRightPos().getValue();

        //Change x and y position of the Object to resize
        this.selectedObject.setPos(tempPosX, posY);

        //DEBUG
        //System.out.println("TOP RIGHT resize.    Mouse X: " + posX + ", Mouse Y: " + posY);

        //Change size of the Object to resize
        this.selectedObject.setSize(posX - tempPosX,
                selObjHeight + (tempTopRightY - posY));
    }

    private void bottomLeftResize(double posX, double posY, double selObjWidth, double selObjHeight){

        double tempPosY = this.selectedObject.getPos().getValue();
        double tempBottomLeftX = this.selectedObject.getBottomLeftPos().getKey();

        //Change x and y position of the Object to resize
        this.selectedObject.setPos(posX, tempPosY);

        //DEBUG
        //System.out.println("BOTTOM LEFT resize.    Mouse X: " + posX + ", Mouse Y: " + posY);

        //Change size of the Object to resize
        this.selectedObject.setSize(selObjWidth + (tempBottomLeftX - posX),
                (selObjHeight + (posY - tempPosY)));
    }

    private void bottomRightResize(double posX, double posY){

        double tempPosX = this.selectedObject.getPos().getKey();
        double tempPosY = this.selectedObject.getPos().getValue();

        //DEBUG
        //System.out.println("BOTTOM RIGHT resize.    Mouse X: " + posX + ", Mouse Y: " + posY);

        //Change size of the Object to resize
        this.selectedObject.setSize(posX - tempPosX,
                posY - tempPosY);
    }
}
