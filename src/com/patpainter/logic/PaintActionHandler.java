package com.patpainter.logic;

import com.patpainter.ChangeShapeVisitor;
import com.patpainter.model.shapes.PaintShape;
import com.patpainter.model.shapes.PaintShapes;
import javafx.util.Pair;


/*
    PaintActionHandler handles all the action events which can occur on the PaintShapes in the Canvas
    It can move or resize the PaintShapes.
    pattern: This class is part of the Visitor Pattern, where it represents an implementation of the
    pattern: ChangeShapeVisitor Visitor Interface
 */
public class PaintActionHandler implements ChangeShapeVisitor {

    public PaintActionHandler(){}

    /**
     * Will move the selected PaintObject to a new location on the Canvas
     */
    public void moveObject(PaintShape shapeToMove, Pair<Double, Double> newPos, Pair<Double,
            Double> relativeObjectPos, boolean relativeDragging){

        if(relativeDragging) {

            //Calculate a new position to simulate moving an object from the location where you
            // initially began dragging the Shape Object instead of moving it from the (0, 0) mouse coordinates position
            double newPosX = newPos.getKey() - relativeObjectPos.getKey();
            double newPosY = newPos.getValue() - relativeObjectPos.getValue();

            //Set new position for the object to be moved
            shapeToMove.setPos(newPosX, newPosY);
        }else{
            shapeToMove.setPos(newPos.getKey(), newPos.getValue());
        }

        //Redraw all shapes on the Canvas (aka redraw the whole screen)
        PaintShapes.getInstance().redrawShapes();
    }

    /**
     * Will resize the selected PaintObject to a new size on the Canvas
     */
    public void resizeObject(PaintShape shapeToResize, double posX, double posY){

        PaintResizer.getInstance().resizeShape(posX, posY, shapeToResize);
    }

    /**
     * Will resize the selected PaintObject (which is part of a Group Obejct)
     *  to a new size on the Canvas
     */
    public void resizeGroupObject(PaintShape shapeToResize, double posX, double posY, double width, double height){

        PaintResizer.getInstance().resizeShape(posX, posY, width, height, shapeToResize);
    }
}
