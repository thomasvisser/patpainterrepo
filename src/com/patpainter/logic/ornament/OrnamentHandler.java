package com.patpainter.logic.ornament;

import com.patpainter.commands.CommandManager;
import com.patpainter.commands.OrnamentCommand;
import com.patpainter.helpers.ConversionHelper;
import com.patpainter.logic.SelectionHandler;
import com.patpainter.model.ornament.OrnamentObject;
import com.patpainter.model.shapes.PaintShape;
import com.patpainter.model.shapes.PaintShapes;
import com.patpainter.view.OrnamentView;

public class OrnamentHandler {

    private static final OrnamentHandler instance = new OrnamentHandler();

    public static OrnamentHandler getInstance(){
        return instance;
    }

    private OrnamentHandler(){}

    /**
     * Adds the ornament to the selected PaintShape Object and draws it on the Canvas.
     */
    public boolean addOrnament(String ornamentTitle, OrnamentType type){

        PaintShape selectedShape = SelectionHandler.getInstance().getSelectedObject();

        if(this.checkIfOrnamentExists(selectedShape, type)){

            OrnamentView.setError("Ornament already in place!");
            return false;
        }

        OrnamentCommand addOrnamentCommand = new OrnamentCommand(selectedShape, ornamentTitle, type);
        CommandManager.getInstance().executeCommand(addOrnamentCommand);

        return true;
    }

    /**
     * Checks if the ornamentType to add already exists on the PaintShape Object to add the ornament to
     * @param onShape the PaintShape Object to check
     * @param type the type of Ornament to check existence for
     * @return boolean indicating whether the type of Ornament already exists on the PaintShape object
     */
    public boolean checkIfOrnamentExists(PaintShape onShape, OrnamentType type){

        if(onShape.getClass().equals(OrnamentObject.class)){

            OrnamentObject ornShape = (OrnamentObject) onShape;
            OrnamentType ornShapeType = ornShape.getOrnamentType();

            if(ornShapeType.equals(type)){
                return true;
            }else{

                //Check recursively for any more Ornaments
                if(this.checkIfOrnamentExists(ornShape.getShape(), type)){
                    return true;
                }
            }
        }else{
            return false;
        }
        return false;
    }

    /**
     * Creates and adds an Ornament to the given PaintShape Object
     * @param ornamentShape The PaintShape Object to add an ornament to
     * @param ornamentTitle The title of the ornament
     * @param type the type of the ornament
     * @return the new PaintShape containing an Ornament
     */
    public PaintShape addOrnamentFromFile(PaintShape ornamentShape, String ornamentTitle, OrnamentType type){

        PaintShape newOrnamentShape = new OrnamentObject(ornamentShape, ornamentTitle, type);

        //Replace new PaintShape with the PaintShape including ornament in the Object List
        PaintShapes.getInstance().replaceShape(ornamentShape, newOrnamentShape);

        //Redraw Shapes on Canvas
        PaintShapes.getInstance().redrawShapes();

        return newOrnamentShape;
    }

    /**
     * Checks if a given PaintShape Object is an Ornament.
     * If yes, it needs to recalculate the ornament position.
     * @param shapeToCheck the PaintShape Object to check.
     */
    public void updateOrnaments(PaintShape shapeToCheck){

        //check for ornament
        if(shapeToCheck.getClass().equals(OrnamentObject.class)){
            OrnamentObject ornObject = (OrnamentObject)shapeToCheck;
            ornObject.calculateOrnamentPosition();

            this.updateOrnaments(ornObject.getShape());
        }
    }

    /**
     * Checks a given Shape for Ornaments and converts them to string data
     * @param shape The shape to read ornaments from
     * @param tabsEnabled determines whether it needs to write the data with tabs enabled yes or no
     * @return the string data of the shapes Ornaments
     */
    public String OrnamentToString(PaintShape shape, boolean tabsEnabled){

        String ornamentString = "";

        PaintShape tempShape = shape;
        while(tempShape.getClass().equals(OrnamentObject.class)){

            OrnamentObject tempObject = (OrnamentObject)tempShape;
            String tempOrnType = "";

            switch (tempObject.getOrnamentType()) {
                case TopOrnament:
                    tempOrnType = "top"; break;
                case RightOrnament:
                    tempOrnType = "right"; break;
                case BottomOrnament:
                    tempOrnType = "bottom"; break;
                case LeftOrnament:
                    tempOrnType = "left"; break;
            }

            if(tabsEnabled) {
                ornamentString += ConversionHelper.getInstance().getTabString() + "ornament " + tempOrnType + " \"" +
                        tempObject.getOrnamentName() + "\"" + "\n";
            }else{
                ornamentString += "ornament " + tempOrnType + " \"" +
                        tempObject.getOrnamentName() + "\"" + "\n";
            }

            tempShape = tempObject.getShape();
        }

        return ornamentString;
    }
}
