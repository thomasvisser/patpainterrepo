package com.patpainter.logic.ornament;

/*
    Describes the position an ornament can have
    relative to the PaintShape object
 */
public enum OrnamentType {
    LeftOrnament,
    RightOrnament,
    TopOrnament,
    BottomOrnament,
}
