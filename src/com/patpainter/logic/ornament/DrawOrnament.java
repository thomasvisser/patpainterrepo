package com.patpainter.logic.ornament;

import com.patpainter.DrawingMethod;
import com.patpainter.Main;

/*
    DrawOrnament acts as an implementation class of the DrawingMethod Interface
    It specifies a different implementation based on the shared 'draw' method
    pattern: Acts as the Strategy Pattern implementation class
 */
public class DrawOrnament implements DrawingMethod {

    //pattern: Singleton implementation
    private static final DrawOrnament instance = new DrawOrnament();
    public static DrawOrnament getInstance(){ return instance; }

    private DrawOrnament(){}

    @Override
    public void drawOrnament(String text, double x, double y) {

        Main.mainController.getGraphicsContext().fillText(text, x, y);
    }

    @Override
    public void draw(double x, double y, double width, double height) throws UnsupportedOperationException{
        throw new UnsupportedOperationException();
    }
}
