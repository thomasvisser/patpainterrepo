package com.patpainter.logic;

import com.patpainter.PaintObjectType;
import com.patpainter.model.groups.Group;
import com.patpainter.model.shapes.*;

import java.util.List;

/*
    This class represents an abstraction to create a new Circle, Ellipse, Rectangle or Square.
    pattern: It acts as a Factory Pattern, to create Object instances of Circles, Ellipses, Rectangles and Squares
 */
public class DrawShapeFactory {

    private static final DrawShapeFactory draw = new DrawShapeFactory();

    public static DrawShapeFactory getInstance(){
        return draw;
    }

    //Indicates whether a Group must be created from a current Selection of Objects,
    // or if a Group must be created based on something else (e.g. loading from file)
    private boolean groupFromSelection = true;

    public void setGroupFromSelection(boolean groupFromSelection){
        this.groupFromSelection = groupFromSelection;
    }

    private DrawShapeFactory(){}

    public PaintShape DrawGroupObject(PaintObjectType typeObject, List<PaintShape> groupObjects){

        switch(typeObject){
            case GROUP:
                return this.createGroup(groupObjects);
        }

        return null;
    }

    public PaintShape DrawPaintObject(PaintObjectType typeObject, double x, double y, double width, double height){

        switch(typeObject){
            case CIRCLE:
            case ELLIPSE:
                return this.drawEllipse(x, y, width, height);
            case SQUARE:
            case RECTANGLE:
                return this.drawRectangle(x, y, width, height);
        }

        return null;
    }

    private PaintShape drawEllipse(double x, double y, double width, double height){

        BaseShape newPaintEllipse = new BaseShape(x, y, width, height, PaintObjectType.ELLIPSE);

        //pattern: Client use of Strategy Pattern
        newPaintEllipse.draw(DrawEllipse.getInstance());

        this.addShapeToModel(newPaintEllipse);

        return newPaintEllipse;
    }

    private PaintShape drawRectangle(double x, double y, double width, double height){

        BaseShape newRectangle = new BaseShape(x, y, width, height, PaintObjectType.RECTANGLE);

        //pattern: Client use of Strategy Pattern
        newRectangle.draw(DrawRectangle.getInstance());

        this.addShapeToModel(newRectangle);

        return newRectangle;
    }

    private PaintShape createGroup(List<PaintShape> groupObjects){

        Group group = new Group();

        for (PaintShape shape : groupObjects) {

            group.addShape(shape);

            this.removeShapeFromModel(shape);
        }

        //Remove all the currently Selected Objects
        SelectionHandler.getInstance().deselectCurrentSelectedObjects(false);

        //Set Group as Selected
        group.setShapeSelected(true, false);
        SelectionHandler.getInstance().addSelectedObject(group);

        //Calculate the position of the Shape relative to the Group.
        //This is needed for positioning calculations to move a whole Group.
        group.calculateRelativeObjectPos();

        //Calculate the position and size of the Shape relative to the Group in percentage points.
        //This is needed for positioning calculations to resize a whole group, with its
        // inner objects
        group.calculateRelativeObjectPosPer();
        group.calculateRelativeObjectSizePer();

        this.addShapeToModel(group);

        return group;
    }

    /**
     * Adds a Shape to the List with PaintShape Objects
     * @param shape the PaintShape to add to the model
     */
    private void addShapeToModel(PaintShape shape){

        //Add Object to the Model
        PaintShapes.getInstance().addShape(shape);
        PaintShapes.getInstance().redrawShapes();
    }

    /**
     * Removes a Shape from the List with PaintShape Objects
     * @param shape the PaintShape to remove from the model
     */
    private void removeShapeFromModel(PaintShape shape){

        //Remove Object from Model;
        PaintShapes.getInstance().removeShape(shape);
    }
}
