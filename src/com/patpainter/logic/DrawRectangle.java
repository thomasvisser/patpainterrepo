package com.patpainter.logic;

import com.patpainter.DrawingMethod;
import com.patpainter.Main;

/*
    DrawRectangle acts as an implementation class of the DrawingMethod Interface
    It specifies a different implementation based on the shared 'draw' method
    pattern: Acts as the Strategy Pattern implementation class
 */
public class DrawRectangle implements DrawingMethod {

    //pattern: Singleton implementation
    private static final DrawRectangle instance = new DrawRectangle();
    public static DrawRectangle getInstance(){ return instance; }

    private DrawRectangle(){}

    @Override
    public void draw(double x, double y, double width, double height) {

        Main.mainController.getGraphicsContext().strokeRect(x, y, width, height);
    }

    @Override
    public void drawOrnament(String text, double x, double y) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }
}
