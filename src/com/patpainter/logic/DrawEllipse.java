package com.patpainter.logic;

import com.patpainter.DrawingMethod;
import com.patpainter.Main;

/*
    DrawEllipse acts as an implementation class of the DrawingMethod Interface
    It specifies a different implementation based on the shared 'draw' method
    pattern: Acts as the Strategy Pattern implementation class
 */
public class DrawEllipse implements DrawingMethod {

    //pattern: Singleton implementation
    private static final DrawEllipse instance = new DrawEllipse();
    public static DrawEllipse getInstance(){ return instance; }

    private DrawEllipse(){}

    @Override
    public void draw(double x, double y, double width, double height) {

        Main.mainController.getGraphicsContext().strokeOval(x, y, width, height);
    }

    @Override
    public void drawOrnament(String text, double x, double y) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }
}
