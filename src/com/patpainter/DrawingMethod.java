package com.patpainter;

/*
    DrawingMethod Interface defines a method 'draw', which will be shared by the implementation
    classes, but will perform different calculations on it.
    pattern: This Class acts as the base Strategy pattern Class
 */
public interface DrawingMethod {

    void draw(double x, double y, double width, double height) throws UnsupportedOperationException;
    void drawOrnament(String text, double x, double y) throws UnsupportedOperationException;
}
