package com.patpainter;

/*
    The public Enum which describes all the paint objects which are
    available to paint on the Canvas
 */
public enum PaintObjectType {
    NONE,
    ELLIPSE,
    CIRCLE,
    RECTANGLE,
    SQUARE,
    GROUP,
}
