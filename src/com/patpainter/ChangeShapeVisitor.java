package com.patpainter;

import com.patpainter.model.shapes.PaintShape;
import javafx.util.Pair;

/*
    pattern: This Class acts as a Visitor Class for the Visitor Pattern.
    pattern: It represents the Base Visitor Class which can be implemented by a Concrete Class
    pattern: to implement moving and resizing functionality.
    pattern: PaintActionHandler will act as the Visitor Implementation class.
 */
public interface ChangeShapeVisitor {

    void moveObject(PaintShape shapeToMove, Pair<Double, Double> newPos, Pair<Double,
            Double> relativeObjectPos, boolean relativeDragging);
    void resizeObject(PaintShape shapeToResize, double posX, double posY);
    void resizeGroupObject(PaintShape shapeToResize, double posX, double posY, double width, double height);
}
