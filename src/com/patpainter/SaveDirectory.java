package com.patpainter;

/*
    Public Enum which describes all the Directories where a file
    can be saved to.
 */
public enum SaveDirectory {
    NONE,
    DOWNLOADS,
    DOCUMENTS,
    DESKTOP,
}
