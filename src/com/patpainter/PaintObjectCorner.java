package com.patpainter;

public enum PaintObjectCorner {
    NONE,
    TOPLEFT,
    TOPRIGHT,
    BOTTOMLEFT,
    BOTTOMRIGHT,
}
