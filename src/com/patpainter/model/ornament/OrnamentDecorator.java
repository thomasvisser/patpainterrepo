package com.patpainter.model.ornament;

import com.patpainter.ChangeShapeVisitor;
import com.patpainter.logic.ornament.DrawOrnament;
import com.patpainter.logic.ornament.OrnamentType;
import com.patpainter.model.shapes.PaintShape;
import javafx.scene.text.Text;
import javafx.util.Pair;

/*
    pattern: This class acts as the Decorator Class for the Decorator Design Pattern.
    pattern: It is the base class for the pattern to add the Ornament functionality to the PaintShapes
    pattern: This class also acts as a leaf for the Composite Pattern
 */
public abstract class OrnamentDecorator extends PaintShape {

    protected PaintShape shape;
    protected String ornament;
    protected OrnamentType type;

    protected double ornX;
    protected double ornY;

    public OrnamentDecorator(PaintShape shape, String ornament, OrnamentType type){
        this.shape = shape;
        this.ornament = ornament;
        this.type = type;

        this.calculateOrnamentPosition();
    }

    public String getOrnamentName(){
        return this.ornament;
    }

    public OrnamentType getOrnamentType(){
        return this.type;
    }

    public PaintShape getShape(){
        return this.shape;
    }

    public void draw(){

        //pattern: Client use of Strategy Pattern
        this.shape.draw(DrawOrnament.getInstance());
    }

    @Override
    public void move(Pair<Double, Double> newPos, Pair<Double, Double> relativeObjectPos, boolean relativeDragging,
                     ChangeShapeVisitor visitor) {

        this.shape.move(newPos, relativeObjectPos, relativeDragging, visitor);

        this.calculateOrnamentPosition();
    }

    public void resizeOrnament(double newX, double newY, ChangeShapeVisitor visitor) {

        this.shape.resize(newX, newY, visitor);

        this.calculateOrnamentPosition();
    }

    public void calculateOrnamentPosition(){

        //Temporarily store the Ornament String in a Text Object to
        // calculate the display width and height of the String
        Text tempOrn = new Text(this.ornament);
        double ornWidth = tempOrn.getLayoutBounds().getWidth();
        double ornHeight = tempOrn.getLayoutBounds().getHeight();

        //Calculate position of the ornament
        switch(this.type){
            case TopOrnament:
                this.ornX = this.shape.getPos().getKey() + (this.shape.getSize().getKey()/2) - (ornWidth/2);
                this.ornY = this.shape.getPos().getValue() - 10;
                break;
            case RightOrnament:
                this.ornX = this.shape.getPos().getKey() + this.shape.getSize().getKey() + 10;
                this.ornY = this.shape.getPos().getValue() + (this.shape.getSize().getValue()/2) + (ornHeight/2);
                break;
            case BottomOrnament:
                this.ornX = this.shape.getPos().getKey() + (this.shape.getSize().getKey()/2) - (ornWidth/2);
                this.ornY = this.shape.getPos().getValue() + this.shape.getSize().getValue() + 15;
                break;
            case LeftOrnament:
                this.ornX = this.shape.getPos().getKey() - 10 - ornWidth;
                this.ornY = this.shape.getPos().getValue() + (this.shape.getSize().getValue()/2) + (ornHeight/2);
                break;
            default:
        }
    }
}
