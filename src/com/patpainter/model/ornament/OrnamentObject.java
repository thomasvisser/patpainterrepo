package com.patpainter.model.ornament;

import com.patpainter.ChangeShapeVisitor;
import com.patpainter.DrawingMethod;
import com.patpainter.PaintObjectCorner;
import com.patpainter.PaintObjectType;
import com.patpainter.logic.ornament.DrawOrnament;
import com.patpainter.logic.ornament.OrnamentHandler;
import com.patpainter.logic.ornament.OrnamentType;
import com.patpainter.model.shapes.PaintShape;
import com.patpainter.model.shapes.PaintShapes;
import com.patpainter.model.shapes.SelectionShape;
import javafx.util.Pair;
import org.javatuples.Quartet;

/*
    This class acts as the concrete implementation of the OrnamentDecorator base class.
    pattern: It is a wrapper for the PaintShape objects, to add Ornament functionality regarding the
    pattern: Decorator Pattern
 */
public class OrnamentObject extends OrnamentDecorator {

    public OrnamentObject(PaintShape shape, String orn, OrnamentType type){
        super(shape, orn, type);
    }

    @Override
    public Pair<Double, Double> getPos() {
        return new Pair<>(this.shape.getPos().getKey(), this.shape.getPos().getValue());
    }

    @Override
    public Pair<Double, Double> getSize() {
        return new Pair<>(this.shape.getSize().getKey(), this.shape.getSize().getValue());
    }

    @Override
    public double getCornerMargin() {
        return this.shape.getCornerMargin();
    }

    @Override
    public PaintObjectType getTypeObject() {
        return this.shape.getTypeObject();
    }

    @Override
    public Pair<Double, Double> getRelativeGroupPos() {
        return this.shape.getRelativeGroupPos();
    }

    @Override
    public Pair<Double, Double> getRelativeGroupPosPer() {
        return this.relativeGroupPosPer;
    }

    @Override
    public Pair<Double, Double> getRelativeGroupSizePer() {
        return this.relativeGroupSizePer;
    }

    @Override
    public SelectionShape getSelectionShape() {
        return this.shape.getSelectionShape();
    }

    @Override
    public void setPos(double newX, double newY) {
        this.shape.setPos(newX, newY);

        OrnamentHandler.getInstance().updateOrnaments(this);
    }

    @Override
    public void setSize(double newWidth, double newHeight) {
        this.shape.setSize(newWidth, newHeight);
    }

    @Override
    public void setRelativeGroupDistance(double x, double y) {
        this.shape.setRelativeGroupDistance(x, y);
    }

    @Override
    public void setRelativeGroupDistancePer(double x, double y) { this.relativeGroupPosPer = new Pair<>(x, y); }

    @Override
    public void setRelativeGroupSizePer(double x, double y) { this.relativeGroupSizePer = new Pair<>(x, y); }

    @Override
    public void setSelectionShape(SelectionShape newShape) {
        this.shape.setSelectionShape(newShape);
    }

    @Override
    public void draw(DrawingMethod method){

        this.shape.draw(method);

        this.drawOrnament();
    }

    @Override
    public void resize(double newX, double newY, ChangeShapeVisitor visitor) {
        visitor.resizeObject(this, newX, newY);

        this.resizeOrnament(newX, newY, visitor);
    }

    @Override
    public void resizeInnerGroupObj(ChangeShapeVisitor visitor, double newPosX, double newPosY, double newSizeW, double newSizeH) {
        visitor.resizeGroupObject(this, newPosX, newPosY, newSizeW, newSizeH);

        this.resizeOrnament(newPosX, newPosY, visitor);
    }

    @Override
    public Pair<Double, Double> getTopLeftPos() {
        return new Pair<>(this.shape.getPos().getKey(), this.shape.getPos().getValue());
    }

    @Override
    public Pair<Double, Double> getTopRightPos() {
        return new Pair<>(this.shape.getPos().getKey() + this.shape.getSize().getKey(), this.shape.getPos().getValue());
    }

    @Override
    public Pair<Double, Double> getBottomLeftPos() {
        return new Pair<>(this.shape.getPos().getKey(), this.shape.getPos().getValue() + this.shape.getSize().getValue());
    }

    @Override
    public Pair<Double, Double> getBottomRightPos() {
        return new Pair<>(this.shape.getPos().getKey() + this.shape.getSize().getKey(), this.shape.getPos().getValue() + this.shape.getSize().getValue());
    }

    @Override
    public double getLeftSideX() {
        return this.shape.getPos().getKey();
    }

    @Override
    public double getRightSideX() {
        return this.shape.getPos().getKey() + this.shape.getSize().getKey();
    }

    @Override
    public double getTopSideY() {
        return this.shape.getPos().getValue();
    }

    @Override
    public double getBottomSideY() {
        return this.shape.getPos().getValue() + this.shape.getSize().getValue();
    }

    @Override
    public Quartet<Double, Double, Double, Double> getCornerMargin(PaintObjectCorner corner) {

        //Layout for Quartet<Double, Double, Double, Double = <Left side, Right side, Top, Bottom> of
        // square Margin Area

        switch(corner)
        {
            case TOPLEFT:
                double leftMarginTL = this.shape.getTopLeftPos().getKey() - this.shape.getCornerMargin();
                double rightMarginTL = this.shape.getTopLeftPos().getKey() + this.shape.getCornerMargin();
                double topMarginTL = this.shape.getTopLeftPos().getValue() - this.shape.getCornerMargin();
                double bottomMarginTL = this.shape.getTopLeftPos().getValue() + this.shape.getCornerMargin();

                return new Quartet<>(leftMarginTL, rightMarginTL, topMarginTL, bottomMarginTL);
            case TOPRIGHT:
                double leftMarginTR = this.shape.getTopRightPos().getKey() - this.shape.getCornerMargin();
                double rightMarginTR = this.shape.getTopRightPos().getKey() + this.shape.getCornerMargin();
                double topMarginTR = this.shape.getTopRightPos().getValue() - this.shape.getCornerMargin();
                double bottomMarginTR = this.shape.getTopRightPos().getValue() + this.shape.getCornerMargin();

                return new Quartet<>(leftMarginTR, rightMarginTR, topMarginTR, bottomMarginTR);
            case BOTTOMLEFT:
                double leftMarginBL = this.shape.getBottomLeftPos().getKey() - this.shape.getCornerMargin();
                double rightMarginBL = this.shape.getBottomLeftPos().getKey() + this.shape.getCornerMargin();
                double topMarginBL = this.shape.getBottomLeftPos().getValue() - this.shape.getCornerMargin();
                double bottomMarginBL =this. shape.getBottomLeftPos().getValue() + this.shape.getCornerMargin();

                return new Quartet<>(leftMarginBL, rightMarginBL, topMarginBL, bottomMarginBL);
            case BOTTOMRIGHT:
                double leftMarginBR = this.shape.getBottomRightPos().getKey() - this.shape.getCornerMargin();
                double rightMarginBR = this.shape.getBottomRightPos().getKey() + this.shape.getCornerMargin();
                double topMarginBR = this.shape.getBottomRightPos().getValue() - this.shape.getCornerMargin();
                double bottomMarginBR = this.shape.getBottomRightPos().getValue() + this.shape.getCornerMargin();

                return new Quartet<>(leftMarginBR, rightMarginBR, topMarginBR, bottomMarginBR);
        }

        return null;
    }

    @Override
    public void setShapeSelected(boolean selected, boolean redrawScreen) {

        if(selected) {
            this.shape.setSelectionShape(new SelectionShape(this.shape.getTopLeftPos(),
                    this.shape.getTopRightPos(), this.shape.getBottomLeftPos(),
                    this.shape.getBottomRightPos(), this.shape.getCornerMargin()));

            this.shape.getSelectionShape().draw();
        }else{

            if(this.shape.getSelectionShape() != null){

                this.shape.setSelectionShape(null);

                if(redrawScreen) {
                    PaintShapes.getInstance().redrawShapes();
                }
            }
        }
    }

    @Override
    public boolean checkMouseIsInBounds(double mouseX, double mouseY) {

        //DEBUG
        //System.out.println("MouseX: " + mouseX + " MouseY: " + mouseY);
        //System.out.println("Left: " + this.getLeftSideX() + " Right: " + this.getRightSideX() + " Top " +
        //    this.getTopSideY() + " Bottom: " + this.getBottomSideY());

        //Check if cursor is within the bounds of the PaintShape object
        if((mouseX >= this.shape.getLeftSideX() && mouseX <= this.shape.getRightSideX()) &&
                (mouseY >= this.shape.getTopSideY() && mouseY <= this.shape.getBottomSideY())){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public PaintObjectCorner checkMouseIsInCornerBounds(double mouseX, double mouseY, PaintObjectCorner cornerType) {

        //Check if mouse position is within bounds of the
        // "cornerType" (TopLeft, TopRight, BottomLeft, BottomRight) Corner of a PaintShape Object
        if(((mouseX >= this.shape.getCornerMargin(cornerType).getValue0()) && (mouseX <= this.shape.getCornerMargin(cornerType).getValue1())) &&
                (mouseY >= this.shape.getCornerMargin(cornerType).getValue2()) && (mouseY <= this.shape.getCornerMargin(cornerType).getValue3())){

            return cornerType;
        }

        return PaintObjectCorner.NONE;
    }

    //Draws the ornament on the left side of the PaintShape
    private void drawOrnament(){
        DrawOrnament.getInstance().drawOrnament(this.ornament, this.ornX, this.ornY);
    }
}
