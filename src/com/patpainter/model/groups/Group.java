package com.patpainter.model.groups;

import com.patpainter.*;
import com.patpainter.helpers.ConversionHelper;
import com.patpainter.DrawingMethod;
import com.patpainter.logic.DrawEllipse;
import com.patpainter.logic.DrawRectangle;
import com.patpainter.logic.ornament.OrnamentHandler;
import com.patpainter.model.ornament.OrnamentObject;
import com.patpainter.model.shapes.PaintShape;
import com.patpainter.model.shapes.PaintShapes;
import com.patpainter.model.shapes.SelectionShape;
import javafx.util.Pair;
import org.javatuples.Quartet;

import java.util.ArrayList;
import java.util.List;

/*
    Base Group class. Class extends from PaintShape.
    This class represents a Group with a list of PaintShape Objects.
    pattern: It acts as the Composite Pattern Class, where PaintShape is the base (root) class,
    pattern: the BaseShape is the leaf, and this Group class is the composite class
 */
public class Group extends PaintShape {

    private List<PaintShape> groupObjects = new ArrayList<>();

    //note: Hacky implementation
    //Temporary variable which stores a PaintShape Object to resize
    private PaintShape tempResizeObject;

    public int getAmountShapes(){ return groupObjects.size(); }

    public Group(){

        this.x = 0;
        this.y = 0;
        this.width = 0;
        this.height = 0;

        this.typeObject = PaintObjectType.GROUP;
    }

    public void addShape(PaintShape shape) {

        this.groupObjects.add(shape);

        //Determine the position and size of the Group
        this.determineGroupBounds();
    }

    public List<PaintShape> getGroupObjects(){
        return this.groupObjects;
    }

    @Override
    public PaintShape getShape() {
        return this;
    }

    @Override
    public Pair<Double, Double> getPos() {
        return new Pair<>(this.x, this.y);
    }

    @Override
    public Pair<Double, Double> getSize() {
        return new Pair<>(this.width, this.height);
    }

    @Override
    public double getCornerMargin() {
        return this.cornerMargin;
    }

    @Override
    public PaintObjectType getTypeObject() {
        return this.typeObject;
    }

    @Override
    public Pair<Double, Double> getRelativeGroupPos() {
        return this.relativeGroupPos;
    }

    @Override
    public Pair<Double, Double> getRelativeGroupPosPer() {
        return this.relativeGroupPosPer;
    }

    @Override
    public Pair<Double, Double> getRelativeGroupSizePer() {
        return this.relativeGroupSizePer;
    }

    @Override
    public SelectionShape getSelectionShape() {
        return this.selectionShape;
    }

    @Override
    public void setPos(double newX, double newY) {
        this.x = newX; this.y = newY;

        this.updateGroupObjects();
    }

    @Override
    public void setSize(double newWidth, double newHeight) {
        this.width = newWidth; this.height = newHeight;

        this.updateGroupObjects();
    }

    @Override
    public void setRelativeGroupDistance(double x, double y) {
        this.relativeGroupPos = new Pair<>(x, y);
    }

    @Override
    public void setRelativeGroupDistancePer(double x, double y) { this.relativeGroupPosPer = new Pair<>(x, y); }

    @Override
    public void setRelativeGroupSizePer(double x, double y) { this.relativeGroupSizePer = new Pair<>(x, y); }

    @Override
    public void setSelectionShape(SelectionShape newShape) {
        this.selectionShape = newShape;
    }

    //Note: Unused DrawingMethod method for Group!
    @Override
    public void draw(DrawingMethod method) {

        for(PaintShape shape : this.groupObjects) {

            if(shape.getTypeObject().equals(PaintObjectType.ELLIPSE)){
                shape.draw(DrawEllipse.getInstance());
            }else if(shape.getTypeObject().equals(PaintObjectType.RECTANGLE)){
                shape.draw(DrawRectangle.getInstance());
            }else if(shape.getTypeObject().equals(PaintObjectType.GROUP)){
                shape.draw(null);
            }
        }
    }

    @Override
    public void move(Pair<Double, Double> newPos, Pair<Double, Double> relativeObjectPos, boolean relativeDragging,
                     ChangeShapeVisitor visitor) {

        visitor.moveObject(this, newPos, relativeObjectPos, relativeDragging);

        for(PaintShape shape : this.groupObjects){

            shape.move(new Pair<>((this.x + shape.getRelativeGroupPos().getKey()),
                    (this.y + shape.getRelativeGroupPos().getValue())), null, false, visitor);
        }
    }

    @Override
    public void resize(double newX, double newY, ChangeShapeVisitor visitor) {

        //Resize top Group Container
        visitor.resizeObject(this, newX, newY);

        //Resize all the Object inside the Group Object
        for(PaintShape shape : this.groupObjects){

            double newPosX = this.x + (this.width * shape.getRelativeGroupPosPer().getKey());
            double newPosY = this.y + (this.height * shape.getRelativeGroupPosPer().getValue());
            double newSizeW = this.width * (shape.getRelativeGroupSizePer().getKey());
            double newSizeH = this.height * (shape.getRelativeGroupSizePer().getValue());

            this.tempResizeObject = shape;

            this.resizeInnerGroupObj(visitor, newPosX, newPosY, newSizeW, newSizeH);
        }

        //Recalculate the relative positions of the Objects inside the Group
        this.calculateRelativeObjectPos();
    }

    @Override
    public void resizeInnerGroupObj(ChangeShapeVisitor visitor,
                                    double newPosX, double newPosY, double newSizeW, double newSizeH){

        if(this.tempResizeObject != null) {
            this.tempResizeObject.resizeInnerGroupObj(visitor, newPosX, newPosY, newSizeW, newSizeH);
        }
    }

    @Override
    public Pair<Double, Double> getTopLeftPos() {
        return new Pair<>(this.x, this.y);
    }

    @Override
    public Pair<Double, Double> getTopRightPos() {
        return new Pair<>(this.x + this.width, this.y);
    }

    @Override
    public Pair<Double, Double> getBottomLeftPos() {
        return new Pair<>(this.x, this.y + this.height);
    }

    @Override
    public Pair<Double, Double> getBottomRightPos() {
        return new Pair<>(this.x + this.width, this.y + this.height);
    }

    @Override
    public double getLeftSideX() {
        return this.x;
    }

    @Override
    public double getRightSideX() {
        return this.x + this.width;
    }

    @Override
    public double getTopSideY() {
        return this.y;
    }

    @Override
    public double getBottomSideY() {
        return this.y + this.height;
    }

    @Override
    public boolean checkMouseIsInBounds(double mouseX, double mouseY) {

        //DEBUG
        //System.out.println("MouseX: " + mouseX + " MouseY: " + mouseY);
        //System.out.println("Left: " + this.getLeftSideX() + " Right: " + this.getRightSideX() + " Top " +
        //    this.getTopSideY() + " Bottom: " + this.getBottomSideY());

        //Check if cursor is within the bounds of the PaintShape object
        if((mouseX >= this.getLeftSideX() && mouseX <= this.getRightSideX()) &&
                (mouseY >= this.getTopSideY() && mouseY <= this.getBottomSideY())){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public PaintObjectCorner checkMouseIsInCornerBounds(double mouseX, double mouseY, PaintObjectCorner cornerType) {

        //Check if mouse position is within bounds of the
        // "cornerType" (TopLeft, TopRight, BottomLeft, BottomRight) Corner of a PaintShape Object
        if(((mouseX >= getCornerMargin(cornerType).getValue0()) && (mouseX <= getCornerMargin(cornerType).getValue1())) &&
                (mouseY >= getCornerMargin(cornerType).getValue2()) && (mouseY <= getCornerMargin(cornerType).getValue3())){

            return cornerType;
        }

        return PaintObjectCorner.NONE;
    }

    /**
     * Calculates and generates the bounds for the newly created Group
     * @return the position and size for the group in the following format:
     *        Quartet<X pos, Y pos, width, height>
     */
    public void determineGroupBounds(){

        double tempXObject = Main.sceneWidth;
        double tempYObject = Main.sceneHeight;

        double minWidth = 0;
        double maxWidth = 0;

        double minHeight = 0;
        double maxHeight = 0;

        for(PaintShape shape : this.groupObjects){

            if(shape.getPos().getKey() < tempXObject){

                tempXObject = shape.getPos().getKey();
                minWidth = shape.getPos().getKey();
            }

            if(shape.getPos().getValue() < tempYObject){

                tempYObject = shape.getPos().getValue();
                minHeight = shape.getPos().getValue();
            }

            if(shape.getRightSideX() > maxWidth){
                maxWidth = shape.getRightSideX();
            }

            if(shape.getBottomSideY() > maxHeight){
                maxHeight = shape.getBottomSideY();
            }
        }

        //Set position of Group
        this.x = tempXObject;
        this.y = tempYObject;

        //set Size of Group
        this.width = maxWidth - minWidth;
        this.height = maxHeight - minHeight;
    }

    /**
     * Calculates the position of the PaintShape Objects in the group relative to the Group Object
     */
    public void calculateRelativeObjectPos(){

        for(PaintShape shape : this.groupObjects) {

            double shapeXPos = shape.getPos().getKey();
            double shapeYPos = shape.getPos().getValue();

            double relativePosX = shapeXPos - this.x;
            double relativePosY = shapeYPos - this.y;

            shape.setRelativeGroupDistance(relativePosX, relativePosY);
        }
    }

    public void calculateRelativeObjectPosPer(){

        for(PaintShape shape : this.groupObjects) {

            double shapeXPos = shape.getPos().getKey();
            double shapeYPos = shape.getPos().getValue();

            double relativePosPerX = (shapeXPos - this.x) / this.width;
            double relativePosPerY = (shapeYPos - this.y) / this.height;

            shape.setRelativeGroupDistancePer(relativePosPerX, relativePosPerY);
        }
    }

    public void calculateRelativeObjectSizePer(){

        for(PaintShape shape: this.groupObjects) {

            double shapeWidth = shape.getSize().getKey();
            double shapeHeight = shape.getSize().getValue();

            double relativeSizePerX = shapeWidth / this.width;
            double relativeSizePerY = shapeHeight / this.height;

            shape.setRelativeGroupSizePer(relativeSizePerX, relativeSizePerY);
        }
    }

    public void updateGroupObjects(){

        for(PaintShape shape : this.groupObjects){

            //Set pos of the inner shape to new relative Group position
            shape.setPos(this.x + (this.width * shape.getRelativeGroupPosPer().getKey()),
                        this.y + (this.height * shape.getRelativeGroupPosPer().getValue()));
            shape.setSize(this.width * shape.getRelativeGroupSizePer().getKey(),
                        this.height * shape.getRelativeGroupSizePer().getValue());

            this.calculateRelativeObjectPos();

            OrnamentHandler.getInstance().updateOrnaments(shape);
        }
    }

    public String exportGroupToString(boolean tabsEnabled){

        String groupString;

        if(tabsEnabled) {
            groupString = ConversionHelper.getInstance().getTabString() + "group " + this.getAmountShapes() + "\n";
        }else{
            groupString = "group " + this.getAmountShapes() + "\n";
        }

        //Increment tab
        ConversionHelper.getInstance().incrementTabString();

        for(PaintShape shape : this.groupObjects) {
            groupString += ConversionHelper.getInstance().ConvertShapesToString(shape, true);
        }

        //Decrement tab
        ConversionHelper.getInstance().decrementTabString();

        return groupString;
    }
}
