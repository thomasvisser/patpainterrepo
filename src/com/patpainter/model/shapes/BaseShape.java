package com.patpainter.model.shapes;

import com.patpainter.ChangeShapeVisitor;
import com.patpainter.DrawingMethod;
import com.patpainter.PaintObjectCorner;
import com.patpainter.PaintObjectType;
import javafx.util.Pair;
import org.javatuples.Quartet;

/*
    This class acts as a base shape for the Rectangle and Ellipse Shapes.
    They share the same properties and functionality. Only the drawing is different.
    BaseShape class extends PaintShape class, which contains the shared properties for every
    Shape pattern: but this is handled by the Strategy Pattern.
    pattern: This class also acts as a leaf for the Composite Pattern
 */
public class BaseShape extends PaintShape {

    public BaseShape(double x, double y, double width, double height, PaintObjectType typeShape){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.typeObject = typeShape;
    }

    @Override
    public PaintShape getShape() {
        return this;
    }

    @Override
    public Pair<Double, Double> getPos() {
        return new Pair<>(this.x, this.y);
    }

    @Override
    public Pair<Double, Double> getSize() {
        return new Pair<>(this.width, this.height);
    }

    @Override
    public double getCornerMargin() {
        return this.cornerMargin;
    }

    @Override
    public PaintObjectType getTypeObject(){
        return this.typeObject;
    }

    @Override
    public Pair<Double, Double> getRelativeGroupPos() {
        return this.relativeGroupPos;
    }

    @Override
    public Pair<Double, Double> getRelativeGroupPosPer() {
        return this.relativeGroupPosPer;
    }

    @Override
    public Pair<Double, Double> getRelativeGroupSizePer() {
        return this.relativeGroupSizePer;
    }

    @Override
    public SelectionShape getSelectionShape(){
        return this.selectionShape;
    }

    @Override
    public void setPos(double newX, double newY) {
        this.x = newX; this.y = newY;
    }

    @Override
    public void setSize(double newWidth, double newHeight) {
        this.width = newWidth; this.height = newHeight;
    }

    @Override
    public void setRelativeGroupDistance(double x, double y) {
        this.relativeGroupPos = new Pair<>(x, y);
    }

    @Override
    public void setRelativeGroupDistancePer(double x, double y) { this.relativeGroupPosPer = new Pair<>(x, y); }

    @Override
    public void setRelativeGroupSizePer(double x, double y) { this.relativeGroupSizePer = new Pair<>(x, y); }

    @Override
    public void setSelectionShape(SelectionShape newShape) {
        this.selectionShape = newShape;
    }

    //pattern: The drawing of the Shape is handled via the DrawingMethod class, which acts
    //pattern: as the base Interface Class for the Strategy Pattern
    @Override
    public void draw(DrawingMethod method){
        method.draw(this.x, this.y, this.width, this.height);
    }

    @Override
    public void move(Pair<Double, Double> newPos, Pair<Double, Double> relativeObjectPos, boolean relativeDragging,
                     ChangeShapeVisitor visitor) {
        visitor.moveObject(this, newPos, relativeObjectPos, relativeDragging);
    }

    @Override
    public void resize(double newX, double newY, ChangeShapeVisitor visitor) {
        visitor.resizeObject(this, newX, newY);
    }

    @Override
    public void resizeInnerGroupObj(ChangeShapeVisitor visitor, double newPosX, double newPosY, double newSizeW, double newSizeH) {

        visitor.resizeGroupObject(this, newPosX, newPosY, newSizeW, newSizeH);
    }

    @Override
    public Pair<Double, Double> getTopLeftPos() {
        return new Pair<>(this.x, this.y);
    }

    @Override
    public Pair<Double, Double> getTopRightPos() {
        return new Pair<>(this.x + this.width, this.y);
    }

    @Override
    public Pair<Double, Double> getBottomLeftPos() {
        return new Pair<>(this.x, this.y + this.height);
    }

    @Override
    public Pair<Double, Double> getBottomRightPos() {
        return new Pair<>(this.x + this.width, this.y + this.height);
    }

    @Override
    public double getLeftSideX() {
        return this.x;
    }

    @Override
    public double getRightSideX() {
        return this.x + this.width;
    }

    @Override
    public double getTopSideY() {
        return this.y;
    }

    @Override
    public double getBottomSideY() {
        return this.y + this.height;
    }

    @Override
    public boolean checkMouseIsInBounds(double mouseX, double mouseY) {

        //DEBUG
        //System.out.println("MouseX: " + mouseX + " MouseY: " + mouseY);
        //System.out.println("Left: " + this.getLeftSideX() + " Right: " + this.getRightSideX() + " Top " +
        //    this.getTopSideY() + " Bottom: " + this.getBottomSideY());

        //Check if cursor is within the bounds of the PaintShape object
        if((mouseX >= this.getLeftSideX() && mouseX <= this.getRightSideX()) &&
                (mouseY >= this.getTopSideY() && mouseY <= this.getBottomSideY())){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public PaintObjectCorner checkMouseIsInCornerBounds(double mouseX, double mouseY, PaintObjectCorner cornerType) {

        //Check if mouse position is within bounds of the
        // "cornerType" (TopLeft, TopRight, BottomLeft, BottomRight) Corner of a PaintShape Object
        if(((mouseX >= getCornerMargin(cornerType).getValue0()) && (mouseX <= getCornerMargin(cornerType).getValue1())) &&
                (mouseY >= getCornerMargin(cornerType).getValue2()) && (mouseY <= getCornerMargin(cornerType).getValue3())){

            return cornerType;
        }

        return PaintObjectCorner.NONE;
    }
}
