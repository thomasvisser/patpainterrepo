package com.patpainter.model.shapes;

import com.patpainter.ChangeShapeVisitor;
import com.patpainter.DrawingMethod;
import com.patpainter.PaintObjectCorner;
import com.patpainter.PaintObjectType;
import javafx.util.Pair;
import org.javatuples.Quartet;

/*
    Public Abstract PaintShape class. Acts as an abstraction for different
    kind of shapes. This class will be used to create Circles, Ellipses, Rectangles
    and Square Objects
    pattern: This class acts as the Component (root) class for the Composite Pattern.
 */
public abstract class PaintShape {

    //X and Y positions of the PaintShape
    protected double x;
    protected double y;

    //If PaintShape is part of a Group, this variable will store the position of the PaintShape
    // relative to the Group Object.
    protected Pair<Double, Double> relativeGroupPos;

    //If PaintShape is part of a Group, this variable will store the position of the PaintShape
    // relative to the Group Object in percentage points for the x and y.
    protected Pair<Double, Double> relativeGroupPosPer;

    //If PaintShape is part of a Group, this variable will store the size of the PaintShape
    // relative to the Group Object in percentage points for the width and height.
    protected Pair<Double, Double> relativeGroupSizePer;

    //Width and Height of the PaintShape
    protected double width;
    protected double height;

    //Type of PaintShape Object
    protected PaintObjectType typeObject;

    //The selected Shape Object
    protected SelectionShape selectionShape;

    protected double cornerMargin = 3.0;

    //Return a reference to the current Shape
    public abstract PaintShape getShape();

    //Returns the x and y positions of the Shape Object
    public abstract Pair<Double, Double> getPos();

    //Returns the width and height of the Shape Object
    public abstract Pair<Double, Double> getSize();

    public abstract double getCornerMargin();

    public abstract PaintObjectType getTypeObject();

    public abstract Pair<Double, Double> getRelativeGroupPos();

    public abstract Pair<Double, Double> getRelativeGroupPosPer();

    public abstract Pair<Double, Double> getRelativeGroupSizePer();

    public abstract SelectionShape getSelectionShape();

    public abstract void setPos(double newX, double newY);

    public abstract void setSize(double newWidth, double newHeight);

    public abstract void setRelativeGroupDistance(double x, double y);

    public abstract void setRelativeGroupDistancePer(double x, double y);

    public abstract void setRelativeGroupSizePer(double x, double y);

    public abstract void setSelectionShape(SelectionShape newShape);

    // Actions to perform on a PaintShape Object
    // pattern: These 3 methods acts as the Base methods for the Visitor Pattern.
    //==================================================================================================================
    public abstract void draw(DrawingMethod method) throws UnsupportedOperationException;

    public abstract void move(Pair<Double, Double> newPos, Pair<Double, Double> relativeObjectPos, boolean relativeDragging,
                              ChangeShapeVisitor visitor) throws UnsupportedOperationException;

    public abstract void resize(double newX, double newY, ChangeShapeVisitor visitor) throws UnsupportedOperationException;
    //==================================================================================================================

    //Extra resize function. This one acts as a resize function for all the shapes part of a Group.
    //Because inner shape positions are calculated based on the parent group container, instead of the
    // mouse position, the resizing of inner objects will therefore be treated via a different algorithm.
    public abstract void resizeInnerGroupObj(ChangeShapeVisitor visitor,
                                             double newPosX, double newPosY, double newSizeW, double newSizeH);

    //Helper methods to calculate the corner position for the PaintShape
    public abstract Pair<Double, Double> getTopLeftPos();
    public abstract Pair<Double, Double> getTopRightPos();
    public abstract Pair<Double, Double> getBottomLeftPos();
    public abstract Pair<Double, Double> getBottomRightPos();

    public abstract double getLeftSideX();
    public abstract double getRightSideX();
    public abstract double getTopSideY();
    public abstract double getBottomSideY();

    //Calculate a Corner Margin Area, for every corner of the PaintShape.
    //This area will be used to display the resize Cursor Control.
    public Quartet<Double, Double, Double, Double> getCornerMargin(PaintObjectCorner corner) {

        //Layout for Quartet<Double, Double, Double, Double = <Left side, Right side, Top, Bottom> of
        // square Margin Area
        switch(corner)
        {
            case TOPLEFT:
                double leftMarginTL = getTopLeftPos().getKey() - this.cornerMargin;
                double rightMarginTL = getTopLeftPos().getKey() + this.cornerMargin;
                double topMarginTL = getTopLeftPos().getValue() - this.cornerMargin;
                double bottomMarginTL = getTopLeftPos().getValue() + this.cornerMargin;

                return new Quartet<>(leftMarginTL, rightMarginTL, topMarginTL, bottomMarginTL);
            case TOPRIGHT:
                double leftMarginTR = getTopRightPos().getKey() - this.cornerMargin;
                double rightMarginTR = getTopRightPos().getKey() + this.cornerMargin;
                double topMarginTR = getTopRightPos().getValue() - this.cornerMargin;
                double bottomMarginTR = getTopRightPos().getValue() + this.cornerMargin;

                return new Quartet<>(leftMarginTR, rightMarginTR, topMarginTR, bottomMarginTR);
            case BOTTOMLEFT:
                double leftMarginBL = getBottomLeftPos().getKey() - this.cornerMargin;
                double rightMarginBL = getBottomLeftPos().getKey() + this.cornerMargin;
                double topMarginBL = getBottomLeftPos().getValue() - this.cornerMargin;
                double bottomMarginBL = getBottomLeftPos().getValue() + this.cornerMargin;

                return new Quartet<>(leftMarginBL, rightMarginBL, topMarginBL, bottomMarginBL);
            case BOTTOMRIGHT:
                double leftMarginBR = getBottomRightPos().getKey() - this.cornerMargin;
                double rightMarginBR = getBottomRightPos().getKey() + this.cornerMargin;
                double topMarginBR = getBottomRightPos().getValue() - this.cornerMargin;
                double bottomMarginBR = getBottomRightPos().getValue() + this.cornerMargin;

                return new Quartet<>(leftMarginBR, rightMarginBR, topMarginBR, bottomMarginBR);
        }

        return null;
    }

    //Sets the selected state of the PaintShape Object
    // Set selected to true, it creates a selected shape square box around the selected shape.
    // Set selected to false, it removes the selected shape square box surrounding the selected shape
    public void setShapeSelected(boolean selected, boolean redrawScreen) {

        if(selected) {
            this.selectionShape = new SelectionShape(this.getTopLeftPos(),
                    this.getTopRightPos(), this.getBottomLeftPos(),
                    this.getBottomRightPos(), this.getCornerMargin());

            this.selectionShape.draw();
        }else{

            if(this.selectionShape != null){

                this.selectionShape = null;

                if(redrawScreen) {
                    PaintShapes.getInstance().redrawShapes();
                }
            }
        }
    }

    //Helper method to check if mouse position is in the bounds of a PaintShape Object
    public abstract boolean checkMouseIsInBounds(double mouseX, double mouseY);

    //Helper method to check if mouse position is in the bounds of one of the corners of a PaintShape Object
    public abstract PaintObjectCorner checkMouseIsInCornerBounds(double mouseX, double mouseY, PaintObjectCorner cornerType);
}