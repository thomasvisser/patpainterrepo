package com.patpainter.model.shapes;

import com.patpainter.Main;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

/*
    The SelectionShape Objects represents the rectangle around any object
    which gets selected. the rectangle has 4 little filled "grapples" (squares) at each corner,
    to indicate the object can be resized.

    the "grapple" word refers to a claw or hook used to hold something. In this case it holds the
    mouse to the object, to resize the object.
 */
public class SelectionShape {

    //Stores 4 rectangles which represents the 4 'grapples' (squares) present in
    // each corner of of the selected Object.
    //The grapples are stored and identified in this order:
    //[0] = Top Left grapple
    //[1] = Top Right grapple
    //[2] = Bottom Left grapple
    //[3] = Bottom Right grapple
    private List<Rectangle> grapples = new ArrayList<>();

    private double x;
    private double y;
    private double width;
    private double height;

    private double grappleMargin;
    private final double amountGrapples = 4;

    public SelectionShape(Pair<Double, Double> topLeftPos, Pair<Double, Double> topRightPos,
                          Pair<Double, Double> bottomLeftPos, Pair<Double, Double> bottomRightPos, double margin) {

        //Sets the margin for the 'grapples'
        this.grappleMargin = margin;

        //Set size and position
        this.x = topLeftPos.getKey();
        this.y = topLeftPos.getValue();
        this.width = topRightPos.getKey() - topLeftPos.getKey();
        this.height = bottomLeftPos.getValue() - topLeftPos.getValue();

        //Create the 'Grapples"
        for(int i = 0; i < this.amountGrapples; i++){

            Pair<Double, Double> grapplePos;

            switch (i){
                case 0:
                    grapplePos = topLeftPos;
                    break;
                case 1:
                    grapplePos = topRightPos;
                    break;
                case 2:
                    grapplePos = bottomLeftPos;
                    break;
                case 3:
                    grapplePos = bottomRightPos;
                    break;
                    default:
                        grapplePos = null;
            }

            Rectangle tempGrapple = new Rectangle(grapplePos.getKey() - this.grappleMargin,
                    grapplePos.getValue() - this.grappleMargin, this.grappleMargin * 2.0,
                    this.grappleMargin * 2.0);

            //Set color
            tempGrapple.setFill(Color.RED);
            tempGrapple.setStroke(Color.RED);

            this.grapples.add(tempGrapple);
        }
    }

    //Draws the SelectedShape Object to the Canvas
    public void draw(){

        //Draw Rectangle around the selected Object
        //Main.mainController.getGraphicsContext().strokeRect(this.x, this.y, this.width, this.height);

        //Draw the 4 'grapples' in each corner of the already drawn rectangle
        for(int i = 0; i < this.amountGrapples; i++){

            Rectangle tempGrapple = this.grapples.get(i);

            Main.mainController.getGraphicsContext().strokeRect(tempGrapple.getX(), tempGrapple.getY(),
                    tempGrapple.getWidth(), tempGrapple.getHeight());
        }
    }
}
