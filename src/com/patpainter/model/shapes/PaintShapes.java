package com.patpainter.model.shapes;

import com.patpainter.*;
import com.patpainter.helpers.ConversionHelper;
import com.patpainter.logic.DrawEllipse;
import com.patpainter.logic.DrawRectangle;
import com.patpainter.logic.SelectionHandler;
import com.patpainter.logic.ornament.OrnamentHandler;
import com.patpainter.model.ornament.OrnamentObject;
import com.patpainter.model.groups.Group;

import java.util.ArrayList;
import java.util.List;

/*
    This class contains a List which stores all the Shapes drawn on the Canvas.
    Trough this class it is possible to add Shapes, remove Shapes, and clear all Shapes.
    pattern: This class acts as the Composite Class of the Composite Pattern.
 */
public class PaintShapes {

    private static final PaintShapes instance = new PaintShapes();

    //The base size (width, height, radius) for the PaintShape Objects
    public static final double baseShapeSize = 30.0;

    public static PaintShapes getInstance(){
        return instance;
    }

    private List<PaintShape> paintObjects = new ArrayList<>();

    private PaintShapes(){}

    public void addShape(PaintShape shape){

        this.paintObjects.add(shape);
    }

    public void replaceShape(PaintShape oldShape, PaintShape newShape){

        if(this.paintObjects.contains(oldShape)){

            int index = this.paintObjects.indexOf(oldShape);
            this.paintObjects.set(index, newShape);

            SelectionHandler.getInstance().replaceSelectedShape(oldShape, newShape);
        }
    }

    public void removeShape(PaintShape shape){

        this.paintObjects.remove(shape);
    }

    public void clearShapes(){

        this.paintObjects.clear();
    }

    //Cursor Logic
    //Checks if a given position is in bounds with any of the PaintShape Objects or
    // it checks if a given position is in bounds with any of the corner positions of the
    // PaintShape Objects.
    //<editor-fold>
    public boolean isMouseInBounds(double mouseX, double mouseY){

        for(PaintShape shape : this.paintObjects){

            if(shape.checkMouseIsInBounds(mouseX, mouseY)) return true;
        }

        return false;
    }

    //Return the PaintShape object which has the same location as the current mouse position
    public PaintShape getSelectedShape(double mouseX, double mouseY){

        for(PaintShape shape : this.paintObjects){

            if(shape.checkMouseIsInBounds(mouseX, mouseY)) return shape;
        }

        return null;
    }

    //Checks if mouse position is in one of the corners of a PaintShape Object
    public PaintObjectCorner isMouseInCornerBounds(double mouseX, double mouseY){

        for(PaintShape shape : this.paintObjects){

            if(shape.checkMouseIsInCornerBounds(mouseX, mouseY, PaintObjectCorner.TOPLEFT) == PaintObjectCorner.TOPLEFT)
                return PaintObjectCorner.TOPLEFT;
            else if(shape.checkMouseIsInCornerBounds(mouseX, mouseY, PaintObjectCorner.TOPRIGHT) == PaintObjectCorner.TOPRIGHT)
                return PaintObjectCorner.TOPRIGHT;
            else if(shape.checkMouseIsInCornerBounds(mouseX, mouseY, PaintObjectCorner.BOTTOMLEFT) == PaintObjectCorner.BOTTOMLEFT)
                return PaintObjectCorner.BOTTOMLEFT;
            else if(shape.checkMouseIsInCornerBounds(mouseX, mouseY, PaintObjectCorner.BOTTOMRIGHT) == PaintObjectCorner.BOTTOMRIGHT)
                return PaintObjectCorner.BOTTOMRIGHT;
        }

        return PaintObjectCorner.NONE;
    }

    //Redraws all the PaintShape Objects to the Canvas
    public void redrawShapes(){

        //Clear Canvas GraphicsContext
        Main.mainController.clearCanvas();

        for(PaintShape shape: this.paintObjects){

            if(shape.getTypeObject().equals(PaintObjectType.RECTANGLE))
                shape.draw(DrawRectangle.getInstance());
            else if(shape.getTypeObject().equals(PaintObjectType.ELLIPSE)){
                shape.draw(DrawEllipse.getInstance());
            }else if(shape.getTypeObject().equals(PaintObjectType.GROUP)){
                shape.draw(null);
            }

            if(shape.getSelectionShape() != null){
                shape.setShapeSelected(true, false);
            }
        }
    }

    //Generates a special String representing all PaintShapes with their corresponding data.
    public String exportShapesToString(){

        String paintShapesStringRepresentation = "";

        for(PaintShape shape: paintObjects){

            if(shape.getTypeObject() == PaintObjectType.GROUP){

                if(shape.getClass().equals(OrnamentObject.class)){

                    String ornamentString = OrnamentHandler.getInstance().OrnamentToString(shape, false);
                    paintShapesStringRepresentation += ornamentString;
                }

                Group tempGroup = (Group)shape.getShape();
                String groupString = tempGroup.exportGroupToString(false);

                paintShapesStringRepresentation += groupString;
                continue;
            }

            String shapeString = ConversionHelper.getInstance().ConvertShapesToString(shape, false);
            paintShapesStringRepresentation += shapeString;
        }

        return paintShapesStringRepresentation;
    }

    //</editor-fold>
}
