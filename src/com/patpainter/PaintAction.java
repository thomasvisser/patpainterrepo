package com.patpainter;

public enum PaintAction {
    NONE,
    DRAW,
    SELECT,
    MOVE,
    RESIZE,
}
